import torch
import torchvision
from torch.utils.data import DataLoader, Dataset
import numpy
import decode
from narsil.segmentation.run import loadNet
from narsil.utils.transforms import resizeOneImage, tensorizeOneImage
import matplotlib.pyplot as plt
from skimage.measure import regionprops,label
from skimage.morphology import remove_small_holes, closing
import scipy
import cv2 as cv
import edt

class rotateImageBy90Degrees(object):
    def __init__(self):
        pass
    def __call__(self, phase_image):
        return numpy.rot90(phase_image)

net=loadNet("mixed10epochs_betterscale_contrastAdjusted1.pth",torch.device("cuda:0"))

class MyDataLoader(Dataset):
    def __init__(self):
        pass
    def __len__(self):
        return 1
    def __getitem__(self,idx):
        img_path=f"../brightness_reference/bg_only/Pos6/phase/img_000000{idx:03}.tiff"

        phase_img=decode.utils.img_file_io.read_img(img_path,from_dtype="u16",to_dtype="float32")

        phase_img=phase_img+numpy.random.normal(scale=0.03,size=phase_img.shape).astype(dtype=numpy.float32)
        phase_img=phase_img.clip(0.0,1.0) # clip to [0.0,1.0] range because added noise might exceed 1.0

        phase_img_normalized = (phase_img - phase_img.mean()) / phase_img.std()

        transform = torchvision.transforms.Compose([
            rotateImageBy90Degrees(),
            tensorizeOneImage(1)])

        phase_img_normalized = transform(phase_img_normalized)

        return phase_img_normalized,idx

dataset=MyDataLoader()
dataloader=DataLoader(dataset,batch_size=1, shuffle=False, num_workers=6)
with torch.no_grad():
    for data in dataloader:
        data,idx=tuple(data)
        idx=idx.item()

        raw_mask=net(data.to("cuda:0")).cpu().numpy().squeeze(0).squeeze(0)
        raw_mask=numpy.rot90(raw_mask,k=-1)

        mask=raw_mask>0.7 # arbitrary value choice

        mask_area={"raw":mask.sum()}

        mask=remove_small_holes(mask,area_threshold=150)
        # morphological closing may remove some more holes/dents, but also merges cells together..

        labelled_mask=label(mask)

        mask_area["holes filled"]=mask.sum()

        regions=regionprops(labelled_mask)
        for region in regions:
            micrometer_per_pixel=0.0345**2 # at this stage, the segmentation mask is in the phase contrast image space, which has a pixel size of 34.5nm (contrary to 65nm in the fluorescence images)
            min_area_micrometer=1.0
            min_area_pixels=min_area_micrometer/micrometer_per_pixel

            max_area_micrometer=5
            max_area_pixels=max_area_micrometer/micrometer_per_pixel

            minr, minc, maxr, maxc = region.bbox
            # the assertion below fails!
            # assert region.area==mask[minr:maxr,minc:maxc].sum(), "if this fails, the region area is the area of the bounding box, not the area of the mask!"

            cell_area=mask[minr:maxr,minc:maxc].sum()
            # enable/disable to filter cells that do not fit size expectations (will also remove those that have accidentally been 'merged')
            if False and ((cell_area<min_area_pixels or cell_area>max_area_pixels)):
                mask_area["size filtered"]=mask.sum()
                mask[minr:maxr,minc:maxc]=0

        mask=mask.astype(numpy.uint8) # cannot be bool (binary) for cv.warpPerspective

        img_path=f"../brightness_reference/bg_only/Pos6/fluor580/img_000000{idx:03}.tiff"
        fluor_img=decode.utils.img_file_io.read_img(img_path,from_dtype="u12",to_dtype="u12")

        transformation_matrix=scipy.io.loadmat("../brightness_reference/bg_only/transMatC_3D.mat")["transformationMatrix"]
        transformation_matrix=numpy.linalg.inv(transformation_matrix.T)
        warped_mask=cv.warpPerspective(mask,transformation_matrix,(fluor_img.shape[1],fluor_img.shape[0]))
        warped_mask=warped_mask>0 # convert back to binary/bool

        fluor_img=fluor_img[1130-1041:2080-1041,460:1120].astype(numpy.float32)
        fluor_img-=100 # transfrom from camera units to photons
        fluor_img=fluor_img*(0.3/0.95)
        warped_mask=warped_mask[1130-1041:2080-1041,460:1120]

        # for some reason, the segmentation mask is shifted by one pixel (discovered in calculate_cell_background_noise_distribution.py)
        fluor_img=fluor_img[:-1]
        warped_mask=warped_mask[1:]

print(f"relative area after holes have been filled {mask_area['holes filled']/mask_area['raw']:.4f}")
if "size filtered" in mask_area:
    print(f"relative area after filtering cell size {mask_area['size filtered']/mask_area['holes filled']:.4f}")

plt.figure(figsize=(15,7))

plt.subplot(131)
plt.suptitle("segmentation mask coverage")
plt.imshow(fluor_img*(1-warped_mask))

plt.subplot(132)
plt.suptitle("segmentation mask, in fluo image space")
plt.imshow(warped_mask)

plt.subplot(133)
plt.suptitle("fluorescence image (snippet)")
plt.imshow(fluor_img)

plt.show()