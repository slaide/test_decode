import numpy
from matplotlib import gridspec
import matplotlib.pyplot as plt
from skimage.filters import gaussian
from skimage.draw import rectangle, disk
from skimage.measure import label, regionprops
from skimage.morphology import binary_erosion, binary_opening
import edt
from tqdm import tqdm
from typing import Dict
import math

if False:
    plt.figure()
    img=numpy.zeros((20,20))
    img[6,6]=1.0
    img[9,9]=1.0
    img[7,3]=1.0
    plt.subplot(121)
    plt.imshow(img)
    plt.colorbar()
    img=gaussian(img,sigma=2)
    plt.subplot(122)
    plt.imshow(img)
    plt.colorbar()
    plt.show()

if False:
    plt.figure()
    img=numpy.zeros((20,20))

    img[6,6]=1.0
    img[10,12]=0.3
    plt.subplot(121)
    plt.imshow(img)
    plt.colorbar()
    img=gaussian(img,sigma=2)
    img+=numpy.random.normal(scale=0.005,size=img.shape)
    plt.subplot(122)
    plt.imshow(img)
    plt.colorbar()
    plt.show()

if False:
    plt.figure()
    img_0=numpy.zeros((20,20))
    img_1=numpy.zeros((20,20))

    img_0[6,6]=1.0
    img_1[10,12]=1.0

    plt.subplot(121)
    plt.imshow(img_0+img_1)
    plt.colorbar()

    img_0=gaussian(img_0,sigma=2)
    img_1=gaussian(img_1,sigma=5)

    plt.subplot(122)
    plt.imshow(img_0+img_1)#+numpy.random.normal(scale=0.005,size=img_0.shape))
    plt.colorbar()
    plt.show()

if False:
    xs=range(-500,500)
    plt.figure()
    plt.plot(xs,[3+  x    **2/500 for x in xs],color="red",label="gaussian PSF: x/y diameter")
    #plt.plot(xs,[3+ (x+50)**2/500 for x in xs],color="blue",label="astigmatic PSF: x diameter")
    #plt.plot(xs,[3+ (x-50)**2/500 for x in xs],color="green",label="astigmatic PSF: y diameter")
    plt.xlabel("emitter z offset")
    plt.xlim(-250,250)
    plt.ylabel("dot size [px]")
    plt.ylim(0,50)
    plt.legend(loc="upper center")
    plt.show()

if False:
    img=numpy.zeros((7,7))
    img[2:5,2:5]=1
    plt.figure()
    plt.subplot(121)
    plt.imshow(img)
    plt.colorbar(ticks=[0,1])
    plt.subplot(122)
    plt.imshow(edt.edt(img))
    plt.colorbar(ticks=[0,1,2])
    plt.show()

def draw_circle(radius,plot=False):
    img=numpy.zeros((radius*2-1,radius*2-1))
    rr,cc=disk((radius-1,radius-1),radius)
    img[rr,cc]=1
    if plot:
        plt.imshow(img)
        plt.show()
    return img

#def draw_circle_buffered(radius):
#    img=numpy.zeros((radius*4-1,radius*4-1))
#    rr,cc=disk((radius*2-1,radius*2-1),radius)
#    img[rr,cc]=1
#    return img
#
#for i in range(1,20):
#    assert draw_circle(i,False).sum()==draw_circle_buffered(i).sum()

# generate and plot fuzzy pixel coverage of pill shape (cell shape)
# fuzzy -> pixel value is proportional to fraction of pixel area covered by shape (i.e. 1 inside the shape, and in range (0;1) on the edge)
def monte_carlo_2d():
    radius=0.5
    xcenter=0.5
    ycenter=0.5

    #rn: radius=0.5, center=(0,0)
    rn=numpy.random.random((int(1e7),2))-0.5

    rn*=2*radius
    rn[:,0]+=xcenter
    rn[:,1]+=ycenter

    rn_mask=((rn[:,0]-xcenter)**2+(rn[:,1]-ycenter)**2) <= (radius**2)
    rn=rn[rn_mask]

    xedges=numpy.arange(-1,1,0.02)
    yedges=numpy.arange(-1,1,0.02)

    img,_xedges,_yedges=numpy.histogram2d(rn[:,0],rn[:,1],bins=(xedges,yedges))
    plt.imshow(img,extent=(-1,1,-1,1))
    plt.show()

# utility function to generate points in 2d/3d space, pseudo-uniformly distributed within the defined bounding box
def generate_points_in_range(x,y,z=None,num_samples=1e7):
    rn=numpy.random.random((int(num_samples),3 if not z is None else 2))

    assert x[1]>x[0]
    assert y[1]>y[0]

    rn[:,0]*=x[1]-x[0]
    rn[:,0]+=x[0]
    rn[:,1]*=y[1]-y[0]
    rn[:,1]+=y[0]

    if not z is None:
        assert z[1]>z[0]
        rn[:,2]*=z[1]-z[0]
        rn[:,2]+=z[0]

    return rn

# generate and plot cell/pill volume per pixel (unit is cubic pixels)
def monte_carlo_3d(
    radius=0.5,
    length=1.0,
    xcenter=0.5,
    ycenter=0.5,
    zcenter=0.0,
    resolution=0.02,
    num_samples=1e7,
    img_extent=(-1.55,1.55,1.55,-1.55)
):

    left,right,top,bottom=img_extent

    yedges=numpy.arange(left,right+resolution/2,resolution)
    xedges=numpy.arange(bottom,top+resolution/2,resolution)

    left_sphere_xcenter=xcenter-length/2
    right_sphere_xcenter=xcenter+length/2

    #rn: 'radius'=0.5, center=(0,0,0)
    rn_left_sphere  = generate_points_in_range(
                        x=(left_sphere_xcenter-radius ,left_sphere_xcenter),
                        y=(ycenter-radius             ,ycenter+radius),
                        z=(zcenter-radius             ,zcenter+radius),
                        num_samples=num_samples)
    rn_left_sphere_mask=(
        (rn_left_sphere[:,0]-left_sphere_xcenter)**2
        + (rn_left_sphere[:,1]-ycenter)**2
        + (rn_left_sphere[:,2]-zcenter)**2
    ) <= (radius**2)

    rn_left_sphere=rn_left_sphere[rn_left_sphere_mask]
    img_left_sphere,_xedges,_yedges=numpy.histogram2d(rn_left_sphere[:,1],rn_left_sphere[:,0],bins=(xedges,yedges))
    del rn_left_sphere,rn_left_sphere_mask

    rn_cylinder     = generate_points_in_range(
                        x=(left_sphere_xcenter        ,right_sphere_xcenter),
                        y=(ycenter-radius             ,ycenter+radius),
                        z=(zcenter-radius             ,zcenter+radius),
                        num_samples=num_samples)
    rn_cylinder_mask=(
        (rn_cylinder[:,1]-ycenter)**2
        + (rn_cylinder[:,2]-zcenter)**2
    ) <= (radius**2)

    rn_cylinder=rn_cylinder[rn_cylinder_mask]
    img_cylinder,_xedges,_yedges=numpy.histogram2d(rn_cylinder[:,1],rn_cylinder[:,0],bins=(xedges,yedges))
    del rn_cylinder,rn_cylinder_mask

    rn_right_sphere = generate_points_in_range(
                        x=(right_sphere_xcenter       ,right_sphere_xcenter+radius),
                        y=(ycenter-radius             ,ycenter+radius),
                        z=(zcenter-radius             ,zcenter+radius),
                        num_samples=num_samples)
    rn_right_sphere_mask=(
        (rn_right_sphere[:,0]-right_sphere_xcenter)**2
        + (rn_right_sphere[:,1]-ycenter)**2
        + (rn_right_sphere[:,2]-zcenter)**2
    ) <= (radius**2)

    rn_right_sphere=rn_right_sphere[rn_right_sphere_mask]
    img_right_sphere,_xedges,_yedges=numpy.histogram2d(rn_right_sphere[:,1],rn_right_sphere[:,0],bins=(xedges,yedges))
    del rn_right_sphere,rn_right_sphere_mask

    half_sphere_volume=4/3*radius**3*numpy.pi
    cylinder_volume=length*radius**2*numpy.pi

    sampling_volume_size_half_sphere=((radius*2)**3) *0.5
    sampling_volume_size_cylinder=length*((radius*2)**2)

    img=img_left_sphere+img_right_sphere \
        +img_cylinder/sampling_volume_size_half_sphere*sampling_volume_size_cylinder # normalize brightness from cylinder section to a uniform amount of brightness per pixel area covered (use half-sphere volume/area brightness as reference)

    #left,right,top,bottom
    extent=(yedges[0],yedges[-1],xedges[-1],xedges[0])

    return img,extent

if True:
    plt.figure()

    img_0=numpy.zeros((20,20))
    img_1=numpy.zeros((20,20))

    img_0[9,9]=1.5
    img_1[9,9]=1.5

    cell,extent=monte_carlo_3d(4,12,ycenter=2,resolution=1,img_extent=(-10,10,10,-10))
    cell/=cell.max()

    plt.subplot(131)
    plt.imshow(cell)#img_0)
    plt.colorbar()
    
    img_0=gaussian(img_0,sigma=(2,5))
    img_1=gaussian(img_1,sigma=(5,2))

    cell*=img_0.max()

    noise=numpy.random.normal(scale=0.005/4,size=img_0.shape)

    plt.subplot(132)
    plt.imshow(img_0+cell+noise)
    plt.colorbar()
    plt.subplot(133)
    plt.imshow(img_1+cell+noise)
    plt.colorbar()
    plt.show()

def upscale_then_erode_mask(mask,upscale_factor,erosion_distance=0.5,erosion_method=None,opening_instead_of_erosion=False):
    assert erosion_method in [None,"horizontalcross","diagonalcross","block","circle"]

    upscaled_shape=tuple([a*upscale_factor for a in mask.shape])
    mask_upscaled=numpy.zeros(shape=upscaled_shape)
    for i in range(upscale_factor):
        for j in range(upscale_factor):
            mask_upscaled[i::upscale_factor,j::upscale_factor]=mask

    if not erosion_method is None:
        erosion_distance_int=int(erosion_distance*upscale_factor)

        footprint=numpy.zeros((erosion_distance_int,erosion_distance_int),numpy.bool8)
        if erosion_method=="diagonalcross":
            for i in range(erosion_distance_int):
                footprint[i,i]=1
                footprint[erosion_distance_int-1-i,i]=1
        elif erosion_method=="horizontalcross":
            footprint[erosion_distance_int//2,:]=1
            footprint[:,erosion_distance_int//2]=1
        elif erosion_method=="block":
            footprint[:,:]=1
        elif erosion_method=="circle":
            footprint=draw_circle(erosion_distance_int)
        else:
            raise ValueError("implementation error, this is a bug.")

        if opening_instead_of_erosion:
            eroded_mask=binary_opening(mask_upscaled,footprint=footprint)
        else:
            eroded_mask=binary_erosion(mask_upscaled,footprint=footprint)

        mask_upscaled=mask_upscaled*eroded_mask
    
    mask_upscaled=map_mask(mask_upscaled,False)

    mask_downscaled=numpy.zeros_like(mask,dtype=numpy.float32)
    for i in range(upscale_factor):
        for j in range(upscale_factor):
            mask_downscaled+=mask_upscaled[i::upscale_factor,j::upscale_factor]

    mask_downscaled/=numpy.max(mask_downscaled)

    return mask_downscaled

# map edt values to circle (and normalize max to 1)
def map_mask(mask,normalize=False):
    mask_edt=edt.edt(mask,parallel=0)

    radius=mask_edt.max()
    mapped_mask=numpy.sqrt(radius**2-(radius-mask_edt)**2)

    if normalize:
        mapped_mask/=mapped_mask.max()

    return mapped_mask

# calculate and plot difference for different approximations for <cell volume per pixel> based on an initial binary cell mask
def compare_shape_approx(cell_radius_pixels=1.0,cell_length=10.0,upscale_factor=2**4,img_width=80,img_height=50,xoffset=0.5,yoffset=0.5):
    assert img_width%2==0
    assert img_height%2==0
    assert cell_length<=img_width
    assert cell_radius_pixels*2<=img_height
    assert cell_length>=(cell_radius_pixels*2)

    half_image_width=img_width//2
    half_image_height=img_height//2

    img,extent=monte_carlo_3d(
        radius=cell_radius_pixels,
        length=cell_length-2*cell_radius_pixels,
        xcenter=0.5,ycenter=0.5,resolution=1.0,
        img_extent=(-half_image_width,half_image_width,half_image_height,-half_image_height),
        #num_samples=4e6)
        num_samples=4e7)

    mask=img>0
    mask_edt=edt.edt(mask,parallel=0)

    radius=numpy.max(mask_edt)
    mask_edt_mapped=numpy.sqrt(radius**2-(mask_edt-radius)**2)

    if False:
        a_edt=mask_edt
        a_edt[a_edt>0]-=1
        a_edt*=-1
        a_edt+=edt.edt(img<1,parallel=0)

        plt.figure()
        plt.subplot(211)
        plt.title("extended EDT")
        plt.imshow(a_edt)
        plt.colorbar()
        plt.subplot(212)
        plt.title("real volume")
        plt.imshow(img)
        plt.show()

        img_list=[("binary mask",mask),("EDT",mask_edt),("EDT mapped",mask_edt_mapped)]

        plt.figure()

        mask_cell_diameter=mask.sum(axis=0).max()
        print(f"{radius=}, {mask_cell_diameter=}")
        for i_img,(name,img_frame) in enumerate(img_list):
            path_x=img_frame.shape[1]//2
            #path_x=numpy.arange(mask.shape[1])[mask.sum(axis=0)>0][-1]
            path_values=img_frame[5:-5,path_x].copy()
            path_radius=numpy.max(path_values)

            plt.subplot(len(img_list),2,i_img*2+1)
            plt.title(name)
            plt.imshow(img_frame,extent=(-img_width/2,img_width/2,-img_height/2,img_height/2))
            plt.plot(numpy.array([path_x,path_x])-img_width/2,numpy.array([5,img_frame.shape[0]-5])-img_height/2,color="red")
            plt.yticks([-img_height/2,-radius,0,radius,img_height/2])
            plt.xticks([-img_width/2,-img_width/4,0,img_width/4,img_width/2])
            plt.xlabel("x [px]")
            plt.ylabel("y [px]")
            plt.colorbar(ticks=[0,1] if i_img==0 else [0,radius])#.ax.set_yticklabels(["0", "1",])

            subplt=plt.subplot(len(img_list),2,i_img*2+2)

            if i_img==0:
                path_values[numpy.arange(path_values.shape[0])[path_values>0][-1]+1]=1
            else:
                subplt.set_aspect('equal', 'box')

            if i_img==len(img_list)-1:
                angle = numpy.linspace(0, numpy.pi, 150)
                circle_x = path_radius * numpy.cos( angle ) 
                circle_y = path_radius * numpy.sin( angle ) 
                plt.plot(circle_x,circle_y,linestyle="solid",color="lightblue",linewidth=4.0)

            plt.plot(numpy.array(range(5,img_frame.shape[0]-5))-img_height/2,path_values,color="red",ds="steps" if i_img==0 else "default")
            plt.xticks([-radius,0,radius])

            if i_img==0:
                plt.ylim(0,2)
                plt.yticks([0,1])
            else:
                plt.ylim(0,15)
                plt.axhline(radius,linestyle="dashed",color="black")

                plt.yticks([0,radius])

            plt.xlim(-15,15)
            plt.ylabel("pixel value")
            plt.xlabel("y [px]")

        plt.tight_layout()
        plt.show()

    mask_i_u=mask_edt[1:-1,1:-1]
    
    mask_i_u_left=mask_edt         [1:-1 ,:-2]
    mask_i_u_right=mask_edt        [1:-1 ,2:]
    mask_i_u_top=mask_edt          [:-2  ,1:-1]
    mask_i_u_bottom=mask_edt       [2:   ,1:-1]

    mask_i_u_top_left=mask_edt     [:-2  ,:-2]
    mask_i_u_top_right=mask_edt    [:-2  ,2:]
    mask_i_u_bottom_left=mask_edt  [2:   ,:-2]
    mask_i_u_bottom_right=mask_edt [2:  ,2:]

    # the '_u' methods are mostly equal to methods of the same name without the suffix. the difference is that the _u set of methods calculate the mean edt from their neighbors, then map to the circle, while the methods without the _u suffix calculate the mean of the already mapped values. in practice, this has a negligible influence on the result.
    #four_neighbor_avg_u=numpy.zeros_like(img)
    #four_neighbor_avg_u[1:-1,1:-1]=(mask_i_u>0)*(mask_i_u+mask_i_u_left+mask_i_u_right+mask_i_u_top+mask_i_u_bottom).astype(numpy.float32)/5
    #radius_u=numpy.max(four_neighbor_avg_u)
    #four_neighbor_avg_u=numpy.sqrt(radius_u**2-(four_neighbor_avg_u-radius_u)**2)

    #eight_neighbor_avg_u=numpy.zeros_like(img)
    #eight_neighbor_avg_u[1:-1,1:-1]=(mask_i_u>0)*(mask_i_u+mask_i_u_left+mask_i_u_right+mask_i_u_top+mask_i_u_bottom+mask_i_u_top_left+mask_i_u_top_right+mask_i_u_bottom_left+mask_i_u_bottom_right).astype(numpy.float32)/9
    #radius_u=numpy.max(eight_neighbor_avg_u)
    #eight_neighbor_avg_u=numpy.sqrt(radius_u**2-(eight_neighbor_avg_u-radius_u)**2)

    # normalize all masks # actually.. not quite right..?
    img=img/numpy.max(img)
    mask_edt=mask_edt/numpy.max(mask_edt)
    mask_edt_mapped=mask_edt_mapped/numpy.max(mask_edt_mapped)
    #four_neighbor_avg_u=four_neighbor_avg_u/numpy.max(four_neighbor_avg_u)
    #eight_neighbor_avg_u=eight_neighbor_avg_u/numpy.max(eight_neighbor_avg_u)

    mask_i=mask_edt_mapped[1:-1,1:-1]
    
    mask_i_left=mask_edt_mapped         [1:-1 ,:-2]
    mask_i_right=mask_edt_mapped        [1:-1 ,2:]
    mask_i_top=mask_edt_mapped          [:-2  ,1:-1]
    mask_i_bottom=mask_edt_mapped       [2:   ,1:-1]

    mask_i_top_left=mask_edt_mapped     [:-2  ,:-2]
    mask_i_top_right=mask_edt_mapped    [:-2  ,2:]
    mask_i_bottom_left=mask_edt_mapped  [2:   ,:-2]
    mask_i_bottom_right=mask_edt_mapped [2:  ,2:]

    #four_neighbor_avg=numpy.zeros_like(img)
    #four_neighbor_avg[1:-1,1:-1]=(mask_i>0)*(mask_i+mask_i_left+mask_i_right+mask_i_top+mask_i_bottom)/5
    four_neighbor_avg=mask*scipy.ndimage.convolve(mask_edt_mapped, numpy.array([[.0,1.,.0],[1.,1.,1.],[.0,1.,.0]]))/5

    #eight_neighbor_avg=numpy.zeros_like(img)
    #eight_neighbor_avg=(mask_i>0)*(mask_i+mask_i_left+mask_i_right+mask_i_top+mask_i_bottom+mask_i_top_left+mask_i_top_right+mask_i_bottom_left+mask_i_bottom_right)/9
    eight_neighbor_avg=mask*scipy.ndimage.convolve(mask_edt_mapped, numpy.ones((3,3)))/9

    # try different erosion methods:

    mask_downscaled=upscale_then_erode_mask(mask, upscale_factor=16,erosion_method=None)

    mask_downscaled_circle_4=  upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.25, erosion_method="circle")
    mask_downscaled_circle_8=  upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.50, erosion_method="circle")
    mask_downscaled_circle_12= upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.75, erosion_method="circle")

    # replacing erosion with morphological opening has essentially the same effect as not eroding at all, just supersampling -> the super-sampled outline seems to be sufficiently roundish. the main issue is then the accuracy of the edt
    #mask_downscaled_circle_4_o=  upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.25, erosion_method="circle",opening_instead_of_erosion=True)
    #mask_downscaled_circle_8_o=  upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.50, erosion_method="circle",opening_instead_of_erosion=True)
    #mask_downscaled_circle_12_o= upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.75, erosion_method="circle",opening_instead_of_erosion=True)

    # these just perform worse than the circle-shaped erosion
    #mask_downscaled_diag_4=   upscale_then_erode_mask( mask, upscale_factor=16, erosion_distance=0.25, erosion_method="diagonalcross")
    #mask_downscaled_diag_8=   upscale_then_erode_mask( mask, upscale_factor=16, erosion_distance=0.50, erosion_method="diagonalcross")
    #mask_downscaled_diag_12=  upscale_then_erode_mask( mask, upscale_factor=16, erosion_distance=0.75, erosion_method="diagonalcross")

    # these just perform worse than the circle-shaped erosion
    #mask_downscaled_horiz_4=  upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.25, erosion_method="horizontalcross")
    #mask_downscaled_horiz_8=  upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.50, erosion_method="horizontalcross")
    #mask_downscaled_horiz_12= upscale_then_erode_mask(mask, upscale_factor=16, erosion_distance=0.75, erosion_method="horizontalcross")

    mask_approximations=[
        (img,                  "monte carlo"),
        (mask_edt_mapped,      "edt mapped"),
        (four_neighbor_avg,    "4 avg"),
        (eight_neighbor_avg,   "8 avg"),
        #(four_neighbor_avg_u,  "4 avg u"),
        #(eight_neighbor_avg_u, "8 avg u"),

        (mask_downscaled,"ss edt mapped noerode"),
        
        (mask_downscaled_circle_4,"ss edt mapped circle 4"),
        (mask_downscaled_circle_8,"ss edt mapped circle 8"),
        (mask_downscaled_circle_12,"ss edt mapped circle 12"),

        #(mask_downscaled_diag_4,"ss edt mapped diag 4"),
        #(mask_downscaled_diag_8,"ss edt mapped diag 8"),
        #(mask_downscaled_diag_12,"ss edt mapped diag 12"),

        #(mask_downscaled_horiz_4,"ss edt mapped horiz 4"),
        #(mask_downscaled_horiz_8,"ss edt mapped horiz 8"),
        #(mask_downscaled_horiz_12,"ss edt mapped horiz 12"),
    ]

    plot_temp_result=0

    if plot_temp_result:
        nrow=2
        ncol=math.ceil(len(mask_approximations)/2)
        gs = gridspec.GridSpec(nrow, ncol, wspace=0.04, hspace=0.1, top=0.98, bottom=0.02, left=0.02, right=1.0)

        fig,subplots=plt.subplots(nrows=2,ncols=ncol,figsize=(18,5))

    results={}
    for i,(mask_approximation,method) in enumerate(mask_approximations):
        mask_diff=mask_approximation-img
        rmsd=numpy.sqrt(numpy.mean(mask_diff**2))

        if plot_temp_result:
            ax=plt.subplot(gs[i//ncol,i%ncol])

            if i==0:
                ax.set_title("baseline volume")
                imshowed_img=ax.imshow(mask_approximation,extent=extent,vmin=-1.0,vmax=1.0,cmap="gist_gray")
            else:
                #ax.set_title(f"{method} ({rmsd:0.3f=})")
                ax.set_title("("+"abcdefghij"[i-1]+")")
                imshowed_img=ax.imshow(mask_diff,extent=extent,vmin=-1.0,vmax=1.0,cmap="cividis")

            ax.set_yticks([0])
            ax.set_xticks([0])
            fig.colorbar(imshowed_img,ax=ax,ticks=[-1,0,1])

        if method=="monte carlo":
            continue

        results[method]=rmsd

    if plot_temp_result:
        plt.show()

    return results

if __name__=="__main__":
    compare_shape_approx(cell_radius_pixels=11.5,cell_length=60)

    results=[]
    for j in tqdm(numpy.arange(11.1,62.2,5.7),desc="length"):
        for i in tqdm(numpy.arange(5,8,0.376),desc="radius",leave=False):
            if j>(i*2):
                for xo in tqdm(numpy.arange(0.0,0.51,0.1),desc="xoffset",leave=False):
                    for yo in tqdm(numpy.arange(0.0,0.51,0.1),desc="yoffset",leave=False):
                        results.append(compare_shape_approx(cell_radius_pixels=i,cell_length=j,xoffset=xo,yoffset=yo))

    print("\n")

    method_avg:Dict[str,float]={}
    for result in results:
        for method in result:
            if method in method_avg:
                method_avg[method]+=result[method]/len(results)
            else:
                method_avg[method]=result[method]/len(results)

    for method in dict(sorted(method_avg.items(),key=lambda x:x[1])):
        print(f"{method:15}: {method_avg[method]:5.3f}")

    """
ss edt mapped circle 8: 0.021
ss edt mapped circle 12: 0.025
ss edt mapped circle 4: 0.030
ss edt mapped noerode: 0.044
8 avg          : 0.055
4 avg          : 0.061
edt mapped     : 0.075
    """
