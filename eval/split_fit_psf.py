import skimage.io
import matplotlib.pyplot as plt
import numpy
#import skimage
from skimage.measure import label, regionprops
import skimage.filters as filters
import torch
import torch.nn.functional as F

bead_stack_path="/home/patrick/code/test_decode/calibration/25_BQ4393_1.tif"
image_index=70
mid_image_index=50
bead_stack_path="/home/patrick/code/test_decode/calibration/50_BQ4373_1.tif"
image_index=30
mid_image_index=25
bead_stack_path="/home/patrick/code/test_decode/calibration/50_BQ4373_2.tif"
image_index=30
mid_image_index=24

# load image stack
bead_stack_images=skimage.io.imread(bead_stack_path)

# remove noise of image used for psf 'correlation'
image=filters.median(bead_stack_images[image_index])
# remove noise of image used for psf center detction
mid_image=filters.median(bead_stack_images[mid_image_index])

# get primitive dot mask
image_median=numpy.median(mid_image)
image_max=numpy.max(mid_image)

scaled_mid_image=(mid_image-image_median)/(image_max-image_median)
image_mask=scaled_mid_image>0.2

# run 'dot detection'
# a) extract dot center coordinates
# b) save dot image
labelled_mask=label(image_mask)
regions=regionprops(labelled_mask)
psf_region_width=41
psf_regions=numpy.zeros((len(regions),psf_region_width,psf_region_width))
dot_centers=[]
for region_id,region in enumerate(regions):
    x,y=region.centroid

    x=int(x)
    y=int(y)

    try:
        psf_regions[region_id]=image[
            x-psf_region_width//2:x+psf_region_width//2+1,
            y-psf_region_width//2:y+psf_region_width//2+1
        ]

        dot_centers.append((x,y))
    except:
        pass

plt.subplot(121)
plt.imshow(mid_image)
dot_centers=numpy.array(dot_centers)
plt.scatter(dot_centers[:,1],dot_centers[:,0],marker="x",color="red")
plt.subplot(122)
plt.imshow(image_mask)
dot_centers=numpy.array(dot_centers)
plt.scatter(dot_centers[:,1],dot_centers[:,0],marker="x",color="red")
plt.close()

# display dot shapes
image_low=numpy.min(image)
image_high=numpy.max(image)

num_dot_rows=5
num_dot_columns=7
num_simultaneous_dots=num_dot_rows*num_dot_columns
for i in range(0,len(regions),num_simultaneous_dots):
    plt.figure(figsize=(15,10))
    for j in range(i,i+num_simultaneous_dots):
        if j>=len(regions):
            break

        plt.subplot(num_dot_rows,num_dot_columns,j-i+1)
        plt.imshow(psf_regions[j],vmin=image_low,vmax=image_high)
    plt.tight_layout()
    plt.close() # temp ignore

# compare different dot shapes
region_diffs=numpy.zeros((len(regions),len(regions)))

# calc mean square deviation of all pairs of dot shapes
for region_0,_ in enumerate(regions):
    for region_1,_ in enumerate(regions):
        # skip calculation of one half of the symmetric distance matrix
        if region_0>region_1:
            continue

        # only use inner part of the dot (ignore possibly noisy outer region)
        outer_padding=psf_region_width//4
        inner_region_0=psf_regions[region_0][outer_padding:-outer_padding,outer_padding:-outer_padding]
        inner_region_1=psf_regions[region_1][outer_padding:-outer_padding,outer_padding:-outer_padding]
        # calculate mean squared difference as distance metric (low score -> low distance -> very similar)
        region_diff=inner_region_0-inner_region_1
        region_diffs[region_0,region_1]=numpy.mean(region_diff**2)

# group dot shapes together based on similarity metric
# set self similarity to largest distance
diagonal_indices=numpy.arange(region_diffs.shape[0])
# remove entries from similarity matrix with values above some threshold
region_diffs_mask=(region_diffs!=0)
region_diffs[region_diffs_mask]=1/region_diffs[region_diffs_mask] # invert similarity score to make search for next best candidate easier, later
distance_threshold=4.4e-3 # manually determined to make results look good
region_diffs_mask[region_diffs<=distance_threshold]=False
region_diffs[diagonal_indices,diagonal_indices]=numpy.min(region_diffs[region_diffs_mask])
region_diffs_mask[diagonal_indices,diagonal_indices]=True

pairs=[]
regions_accounted_for=numpy.zeros((len(regions)),dtype=bool)
region_diffs_copy=region_diffs.copy()
img_x_dim,img_y_dim=region_diffs_copy.shape
while region_diffs_mask.any():
    region_diffs_copy[~region_diffs_mask]=0
    
    argmax=numpy.argmax(region_diffs_copy)
    argy,argx=argmax//img_y_dim,argmax%img_y_dim
    score=numpy.max(region_diffs_copy)

    assert region_diffs_copy[argy,argx]==score, "conversion from numpy.argmax result to row/column index failed (this is a bug)"

    pairs.append((argx,argy,score))
    regions_accounted_for[argy]=True

    prev_sum=region_diffs_mask.sum()
    region_diffs_mask[argy,:]=0

    assert region_diffs_mask.sum()<prev_sum, f"making sure one element got selected {region_diffs_mask.sum()=} {prev_sum=}"

assert regions_accounted_for.all()

# collaps list of emitters paired together into map
pairs_collapsed={}
for i,j,score in pairs:
    if i not in pairs_collapsed:
        pairs_collapsed[i]=[]
    pairs_collapsed[i].append((j,score))

# setup plotting grid
num_psf_groups=len(pairs_collapsed.keys())
max_num_psf_group_instances=numpy.max([len(entries) for entries in pairs_collapsed.values()])
group_subset=[
    group for group 
    in pairs_collapsed.items() 
    if len(group[1])>1
] # select only groups with at least N (e.g. 2) elements for plotting
fig,axes=plt.subplots(max_num_psf_group_instances,len(group_subset),figsize=(15,50))
# fix plt.subplot's result which returns single elements instead of lists with a single element in case of subplot axis size of one
if max_num_psf_group_instances==1:
    axes=[axes]
if num_psf_groups==1:
    for i in range(num_psf_groups):
        axes[i]=[axes[i]]

# show all dots in all groups (sorted with largest groups at top)
for group_id,(_group_id,group_instances) in enumerate(sorted(
    group_subset,
    key= lambda v: len(v[1]), # sort by number of dots in group
    reverse=True # in descending order
)):
    for instance_id,(instance,score) in enumerate(group_instances):
        ax=axes[instance_id][group_id]
        ax.imshow(psf_regions[instance],vmin=image_low,vmax=image_high)
        ax.set_title(f"{score:7.2e} {'*' if _group_id==instance else ''}")

fig.set_figheight(50)
plt.tight_layout()
plt.show()