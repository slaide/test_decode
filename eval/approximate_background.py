import torch
from torch import cuda
import torchvision
from torch.utils.data import DataLoader, Dataset
import numpy
import decode
from decode.utils.narsil.segmentation.run import loadNet
from decode.utils.narsil.utils.transforms import resizeOneImage, tensorizeOneImage
import matplotlib.pyplot as plt
from skimage.measure import regionprops,label
from skimage.morphology import remove_small_holes
from skimage.filters import gaussian
import scipy
import skimage
import cv2 as cv
import edt
import sys
from tqdm import tqdm
import math
import threading
from multiprocessing import Process, Value
import gc
import time
from numpy.random import default_rng
import glob
import matplotlib
from pathlib import Path

NUM_IMAGES_FOR_CALCULATION=5 # use 5 images for gradient approximation (more images may yield better result, but increase runtime)

project_base_path=Path("/home/patrick/code/test_decode")

param = decode.utils.read_params(str(project_base_path/"calibration/params.yaml"))
#base_path=str(project_base_path/"experiments/BQ4383_bg_only/EXP-22-BQ4399")
base_path=str(project_base_path/"experiments/BQ4383_bg_only/EXP-22-BQ4383")

# segmentation mask net
model_path=str(project_base_path/"calibration/mixed10epochs_betterscale_contrastAdjusted1.pth")

# (glob) directories/folders containing these folders: phase, fluor515, fluor580
# -> glob.glob(pos_dirs) = ["/Run/Pos01","/Run/Pos02","/Run/Pos03", etc.]
pos_dirs=base_path+"/Run/Pos*"

# 601 images per dir (intrinsic knowledge, though not verified here)
num_dirs_required_for_calculation=(NUM_IMAGES_FOR_CALCULATION//601)+(NUM_IMAGES_FOR_CALCULATION%601!=0)

# generate cell masks in appropriate locations (do not regenerate masks if already present)
decode.utils.gen_masks.generate_cell_masks(
    directory_list=glob.glob(pos_dirs)[:num_dirs_required_for_calculation],
    fluo_roi=param.Simulation.fluo_roi,
    fluor_dir_name="fluor515",
    dir_phase="phase",

    fluor_cropped_out_dir_name="fluor_cropped_515",
    dir_warped_dist_masks=(False,"warped_dist_masks_515"),

    transmat_file_name=str(project_base_path/"calibration/transMatV_3D.mat"),
    model_path=model_path
)

# move roi up by half of image height to show roi of other channel
param.Simulation.fluo_roi=(
    param.Simulation.fluo_roi[0],
    (
        param.Simulation.fluo_roi[1][0]-(2082//2),
        param.Simulation.fluo_roi[1][1]-(2082//2)
    ),
)

decode.utils.gen_masks.generate_cell_masks(
    directory_list=glob.glob(pos_dirs)[:num_dirs_required_for_calculation],
    fluo_roi=param.Simulation.fluo_roi,
    fluor_dir_name="fluor580",
    dir_phase="phase",

    fluor_cropped_out_dir_name="fluor_cropped_580",
    dir_warped_dist_masks=(False,"warped_dist_masks_580"),

    transmat_file_name=str(project_base_path/"calibration/transMatC_3D.mat"),
    model_path=model_path
)

""" different channels have different parameters """
# 515nm wavelength channel
fluor_list=glob.glob(f"{pos_dirs}/fluor_cropped_515/*.tiff")
mask_list=glob.glob(f"{pos_dirs}/warped_dist_masks_515/*.tiff")
# 580nm wavelength channel
#fluor_list=glob.glob(f"{pos_dirs}/fluor_cropped_580/*.tiff")
#mask_list=glob.glob(f"{pos_dirs}/warped_dist_masks_580/*.tiff")

camera=decode.simulation.camera.Photon2Camera.parse(param)

assert len(fluor_list)>=NUM_IMAGES_FOR_CALCULATION
assert len(fluor_list)==len(mask_list)

fluor_list=[skimage.io.imread(img,as_gray=True) for img in fluor_list[:NUM_IMAGES_FOR_CALCULATION]]
mask_list=[skimage.io.imread(img,as_gray=True) for img in mask_list[:NUM_IMAGES_FOR_CALCULATION]]

fluor_list=[f.astype(numpy.float32) for f in fluor_list]
fluor_list=[camera.backward(torch.from_numpy(f)).numpy() for f in fluor_list]

def calc_gradient(masks,fluors):
    assert len(masks)==len(fluors)

    # Relevant Pixel MeanS
    rpms={}

    for i in range(len(masks)):
        warped_mask=masks[i]>0
        fluor_img=fluors[i]

        # calculate modified edt
        n_warped_mask=~warped_mask
        fluor_edt  = edt.edt(n_warped_mask.astype(numpy.float32),parallel=0).astype(numpy.int16)
        fluor_edt += edt.edt(warped_mask.astype(numpy.float32),parallel=0).astype(numpy.int16)
        fluor_edt[warped_mask]=-fluor_edt[warped_mask]+1
        assert fluor_edt.min()<0

        # for all values in the edt
        for j in range(fluor_edt.min(),fluor_edt.max()):
            # get all pixels for given edt value
            fluor_edt_mask_j=fluor_edt==j
            relevant_fluor_pixels=fluor_img[fluor_edt_mask_j]

            rps=relevant_fluor_pixels.sum()
            num=len(relevant_fluor_pixels)

            if j in rpms:
                rpms[j]['num']+=num
                rpms[j]['rps']+=rps
            else:
                rpms[j]={'num':num,'rps':rps}

    # check the number of pixels with the given edt value (edt value = j)
    # go through the list of those values, sorted by j, in reverse
    # for those j where the pixel count is at least 40% of the maximum number of pixels for any j
    # calculate delta pixelcount (dp) divided by delta j (where delta j is fixed at 1, since we assume that every j up to the maximum is present)
    # if the dp for the current j is smaller than the dp for the previous j, save the current j
    # this saved j is the maximum j for which the pixel count is assumed to be large enough to average out numerical errors
    rpss={j:rpm["rps"]/rpm["num"] for j,rpm in rpms.items() if j>=-5}

    if False:
        plt.figure(figsize=(15,10),dpi=200)

        plt.plot([j for j in filtered_rpms if j<=rpm_break],[rpm["rps"]/rpm["num"] for j,rpm in filtered_rpms.items() if j<=rpm_break],c="red",label="high")
        plt.plot([j for j in filtered_rpms if j>=rpm_break],[rpm["rps"]/rpm["num"] for j,rpm in filtered_rpms.items() if j>=rpm_break],c="green",label="low")
        plt.ylabel("mean value [photons]")
        plt.ylim(0)
        plt.xlabel("edt value")
        plt.legend(title="frequency of EDT values")

        plt.twinx().plot([j for j in filtered_rpms],numpy.array([rpm['num']/numpy.max([rpm['num'] for rpm in filtered_rpms.values()]) for rpm in filtered_rpms.values()]),c="blue",label="num")
        plt.ylabel("relative frequency",color="blue")
        plt.ylim(0)
        plt.show()

    return {
        'means':{j:rpm['rps']/rpm['num'] for j,rpm in rpms.items() if j>-4 and j<=14},
        'freq' :{j:rpm['num']            for j,rpm in rpms.items() if j>-4 and j<=14}
    }

def simulate_fluors(fluorophore_brightness,blur_radius,environmental_brightness,sample_photon_count=True):
    simulated_images=[
        gaussian(
            img * fluorophore_brightness,
            blur_radius,
            # truncate=100.0, # this effectively makes no difference (Default is 4*std.dev.)
        ) + environmental_brightness
        for img in mask_list
    ]

    # this is optional because it is not required for parameter fitting
    if sample_photon_count:
        for j in range(len(simulated_images)):
            simulated_images[j]=numpy.random.poisson(simulated_images[j])

    #plt.imshow(simulated_images[0])
    #plt.colorbar()
    #plt.show()

    return simulated_images

def minmize_worker_func(bounds,_initial_guess,step_length,target_rmsd,_proc_id,last_min_args,last_min,poison_pill):
    initial_guess=_initial_guess.copy()
    proc_rng=default_rng(_proc_id)
    num_resets=0
    local_step_length=step_length.value

    while last_min.value>target_rmsd:
        # terminate if no new solution has been found after the search has been restarted a certain number of times
        if num_resets>5:
            return

        res=minimize_me(initial_guess)

        if res<last_min.value:
            with step_length.get_lock():
                with last_min.get_lock():
                    step_length.value/=last_min.value/res
                    last_min.value=res
            for i in range(3):
                with last_min_args[i].get_lock():
                    last_min_args[i].value=initial_guess[i]
            num_resets=0
        elif res!=last_min.value:
            initial_guess=numpy.array([v.value for v in last_min_args])
            local_step_length=step_length.value

        if not poison_pill.value:
            next_step=proc_rng.uniform(-local_step_length,local_step_length,size=(3,))

            if False: # normalize to fixed step length
                next_step/=numpy.sqrt(numpy.sum(next_step**2))*local_step_length

            initial_guess+=next_step

            deviated_too_far=numpy.sqrt(numpy.sum((initial_guess-numpy.array([v.value for v in last_min_args]))**2))>local_step_length*20
            hit_boundary=(initial_guess!=numpy.clip(initial_guess, bounds[:,0], bounds[:,1])).any()
            if deviated_too_far or hit_boundary:
                initial_guess=_initial_guess.copy()
                num_resets+=1
                with step_length.get_lock():
                    step_length.value*=0.98
        else:
            break

res_real=calc_gradient(mask_list,fluor_list)
res_real_np=numpy.array([res_real['means'][j] for j in res_real['means']])

def minimize_me(args,do_plot=False,block=True,return_fig:bool=False,reuse_fig=None):
    a,b,c=args

    res_sim0=calc_gradient(mask_list,simulate_fluors(fluorophore_brightness=a,blur_radius=b,environmental_brightness=c,sample_photon_count=False))
    res_sim0_np=numpy.array([res_sim0['means'][j] for j in res_real['means']])
    freqs=numpy.array([res_sim0['freq'][j] for j in res_real['freq']])

    delta=(res_real_np-res_sim0_np)
    diff=delta**2*freqs
    diff=(delta*freqs)**2
    result=numpy.sqrt(numpy.mean(diff))

    if do_plot:
        x_axis=range(len(res_real_np))
        x_axis=[j for j in res_real['means']]

        if reuse_fig:
            fig=reuse_fig
        else:
            fig=plt.figure(figsize=(5,4),dpi=200)
        axes=fig.gca()
        lns1=axes.plot(x_axis,res_real_np,label="real")
        lns2=axes.plot(x_axis,res_sim0_np,label="simulated")
        axes.set_xlabel("distance to cell outline")
        axes.set_ylabel("mean value [photons]")
        axes.set_title(f"rmsd = {result:.3f}")
        axes.legend()
        twinx=axes.twinx()
        lns3=twinx.plot(x_axis,freqs/freqs.max(),label="value freq.",c="green")
        twinx.set_ylabel("f")

        lns = lns1+lns2+lns3
        labs = [l.get_label() for l in lns]
        axes.legend(lns, labs, loc=0)

        plt.show(block=block)

    if return_fig:
        return result,fig
    else:
        return result

# manually determined boundaries
# left to right: fluorophore_brightness in phot/unitless volume, blur_radius (gaussian psf std. dev. for z=0 in px), environmental_brightness in phot/pix
bounds=numpy.array([[0.0,10.0],[1.5,10.0],[0.5,10.0]])

initial_guess=[(b[1]-b[0])/2+b[0] for b in bounds] # start in center of parameter space

initial_guess=numpy.array(initial_guess)

# sample parameter space starting from initial guess within boundaries in all 3 direction (with the other 2 parameters fixed)
if False:
    intitial_guess_result=minimize_me(initial_guess,do_plot=False)
    steps=30
    for p,label in enumerate(tqdm(["fluor","blur","env"])):
        color=["red","blue","green"][p]

        test_values=[]
        
        step_width=(bounds[p][1]-bounds[p][0])/steps
        parameter_range=numpy.arange(bounds[p][0],bounds[p][1],step_width)[:steps]
        for j in tqdm(parameter_range,leave=False):
            current_args=numpy.copy(initial_guess)
            current_args[p]=j
            test_values.append(minimize_me(current_args))

        plt.plot(numpy.arange(steps),test_values,label=label,color=color)
        plt.scatter([(initial_guess[p]-bounds[p][0])/step_width],[intitial_guess_result],color=color)
    plt.title("parameter scan")
    plt.xlabel("# parameter step (within its boundaries)")
    plt.ylabel("rmsd")
    plt.legend()
    plt.show()

num_threads=11 # on 12 core machine, make main thread plot and check for results
terminate_after_unsuccessfull_seconds=5
initial_step_length=0.5

last_min_args=[
    Value('f',initial_guess[i])
    for i in range(3)
]
last_min=Value('f',minimize_me([v.value for v in last_min_args],do_plot=False))
poison_pill=Value('b',False) # not actually a poison pill

step_length=Value('f',initial_step_length)
workers=[
    Process(target=minmize_worker_func,args=(bounds.copy(),initial_guess.copy(),step_length,0.19,j,last_min_args,last_min,poison_pill))
    for j 
    in range(num_threads)]

# make sure to kill worker threads on exception (will survive parent process otherwise)
try:
    for w in workers:
        w.daemon=True
        w.start()

    threads_done=[False for _ in workers]
    last_local_min=last_min.value*2
    fig=None
    current_time=time.time()
    iterations_without_success=0
    while numpy.sum(numpy.array(threads_done))!=num_threads:
        while time.time()-current_time < 1/60:
            if fig is None:
                break

            fig.canvas.draw()
            fig.canvas.flush_events()
        current_time=time.time()

        if last_local_min>last_min.value:
            last_local_min=last_min.value

            print(f"new min: {last_local_min:.2f} ({[v.value for v in last_min_args]}) step_length: {step_length.value:.4f}")

            if not fig is None:
                fig.clf()

                minimize_me([v.value for v in last_min_args],do_plot=True,block=False,return_fig=True,reuse_fig=fig)
                fig.canvas.draw()
                fig.canvas.flush_events()
            else:
                _,fig=minimize_me([v.value for v in last_min_args],do_plot=True,block=False,return_fig=True)
                def kill_search(ev):
                    poison_pill.value=True
                fig.canvas.mpl_connect('close_event', kill_search)

            iterations_without_success=0
        else:
            iterations_without_success+=1

        for j in range(num_threads):
            if not threads_done[j]:
                workers[j].join(0.001)
                if not workers[j].is_alive():
                    workers[j].close()
                    threads_done[j]=True

        if poison_pill.value or iterations_without_success>=terminate_after_unsuccessfull_seconds/(1/60):
            with poison_pill.get_lock():
                poison_pill.value=True

            while numpy.sum(numpy.array(threads_done))!=num_threads:
                for j in range(num_threads):
                    if not threads_done[j]:
                        workers[j].terminate()
                        workers[j].join()
                        if not workers[j].is_alive():
                            workers[j].close()
                            threads_done[j]=True

    print(f"done ; min={last_min.value} ({[v.value for v in last_min_args]})")
except:
    for w in workers:
        w.terminate()
