import decode
from decode.utils.entry import Entry
from tqdm import tqdm
import scipy
import numpy
import matplotlib.pyplot as plt

entry=Entry(
    model_file="/home/patrick/code/test_decode/out/roi45_noiseRemoved_real/model_2.pt",
    param_file="/home/patrick/code/test_decode/konrads_params.yaml",
)

# mean image value is ~210 for image#0, ~150 for #200, ~140 for #999
image_list=range(200,1000,1)
coords_list=[]
for image_index in tqdm(image_list):
    image_adu=decode.utils.img_file_io.read_img(f"/mnt/big_data/code/test_decode/experiments/.membrane_associated/fluor580/img_000000{image_index:03}.tiff",'u12','u12')
    coords=entry.localize(image_adu)

    coords_list.append(coords)

#plt.imshow(image_adu,cmap="jet")
coords_list_numpy=numpy.concatenate([coords.xyz_nm.cpu().numpy() for coords in coords_list])

scipy.io.savemat("membrane_localizations.mat",{"coords_xyz_nm":coords_list_numpy})

def plot_thing(image_axis_0,image_axis_1,step_size):
    x_lim=coords_list_numpy[:,image_axis_0].min(),coords_list_numpy[:,image_axis_0].max()
    y_lim=coords_list_numpy[:,image_axis_1].min(),coords_list_numpy[:,image_axis_1].max()

    x_bins=numpy.arange(*x_lim,step_size)
    y_bins=numpy.arange(*y_lim,step_size)

    hist,_bins_x,_bins_y=numpy.histogram2d(coords_list_numpy[:,image_axis_0],coords_list_numpy[:,image_axis_1],bins=(x_bins,y_bins))

    plt.figure()
    plt.imshow(hist,extent=(y_lim[0],y_lim[1],x_lim[0],x_lim[1]),cmap="jet")
    plt.xlim(*y_lim)
    axis_names=["x (short axis)","y (long axis)","z"]
    plt.xlabel(axis_names[image_axis_1])
    plt.ylim(*x_lim)
    plt.ylabel(axis_names[image_axis_0])
    #plt.scatter(coords.xyz_px.numpy()[:,1],coords.xyz_px.numpy()[:,0],s=10,marker="x",c="black")
    plt.colorbar()
    plt.show()

plot_thing(0,1,50) # xy-plot
plot_thing(2,0,10) # zx-plot
