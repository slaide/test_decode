import decode
from decode.utils.entry import Entry
from tqdm import tqdm
import scipy
import numpy
import torch
import matplotlib.pyplot as plt

entry=Entry(
model_file="/home/patrick/code/test_decode/out/roi45_noiseRemoved_real/model_2.pt",
param_file="/home/patrick/code/test_decode/out/roi45_noiseRemoved_real/param_run_in.yaml",
device="cpu"
)

psf_roi27 = decode.utils.calibration_io.SMAPSplineCoefficient(
    calib_file="/home/patrick/code/test_decode/calibration/zstack_25_1/zStack_roi27.mat"
).init_spline(
    xextent=entry.params.Simulation.psf_extent_img[0],
    yextent=entry.params.Simulation.psf_extent_img[1],
    img_shape=(
        entry.params.Simulation.psf_extent_img[0][1],
        entry.params.Simulation.psf_extent_img[1][1]
    ),
    device=entry.params.Hardware.device_simulation,
    roi_size=entry.params.Simulation.roi_size,
    roi_auto_center=entry.params.Simulation.roi_auto_center
)

psf_roi37 = decode.utils.calibration_io.SMAPSplineCoefficient(
    calib_file="/home/patrick/code/test_decode/calibration/zstack_25_1/zStack_roi37.mat"
).init_spline(
    xextent=entry.params.Simulation.psf_extent_img[0],
    yextent=entry.params.Simulation.psf_extent_img[1],
    img_shape=(
        entry.params.Simulation.psf_extent_img[0][1],
        entry.params.Simulation.psf_extent_img[1][1]
    ),
    device=entry.params.Hardware.device_simulation,
    roi_size=entry.params.Simulation.roi_size,
    roi_auto_center=entry.params.Simulation.roi_auto_center
)

psf_roi45 = decode.utils.calibration_io.SMAPSplineCoefficient(
    calib_file="/home/patrick/code/test_decode/calibration/zstack_25_1/zStack_roi45.mat"
).init_spline(
    xextent=entry.params.Simulation.psf_extent_img[0],
    yextent=entry.params.Simulation.psf_extent_img[1],
    img_shape=(
        entry.params.Simulation.psf_extent_img[0][1],
        entry.params.Simulation.psf_extent_img[1][1]
    ),
    device=entry.params.Hardware.device_simulation,
    roi_size=entry.params.Simulation.roi_size,
    roi_auto_center=entry.params.Simulation.roi_auto_center
)

psf_roi49_d50 = decode.utils.calibration_io.SMAPSplineCoefficient(
    calib_file="/home/patrick/code/test_decode/calibration/zstack_50_1/zStack_roi49.mat"
).init_spline(
    xextent=entry.params.Simulation.psf_extent_img[0],
    yextent=entry.params.Simulation.psf_extent_img[1],
    img_shape=(
        entry.params.Simulation.psf_extent_img[0][1],
        entry.params.Simulation.psf_extent_img[1][1]
    ),
    device=entry.params.Hardware.device_simulation,
    roi_size=entry.params.Simulation.roi_size,
    roi_auto_center=entry.params.Simulation.roi_auto_center
)

psf_roi51 = decode.utils.calibration_io.SMAPSplineCoefficient(
    calib_file="/home/patrick/code/test_decode/calibration/zstack_25_1/zStack_roi51.mat"
).init_spline(
    xextent=entry.params.Simulation.psf_extent_img[0],
    yextent=entry.params.Simulation.psf_extent_img[1],
    img_shape=(
        entry.params.Simulation.psf_extent_img[0][1],
        entry.params.Simulation.psf_extent_img[1][1]
    ),
    device=entry.params.Hardware.device_simulation,
    roi_size=entry.params.Simulation.roi_size,
    roi_auto_center=entry.params.Simulation.roi_auto_center
)

psf_roi63 = decode.utils.calibration_io.SMAPSplineCoefficient(
    calib_file="/home/patrick/code/test_decode/calibration/zstack_25_1/zStack_roi63.mat"
).init_spline(
    xextent=entry.params.Simulation.psf_extent_img[0],
    yextent=entry.params.Simulation.psf_extent_img[1],
    img_shape=(
        entry.params.Simulation.psf_extent_img[0][1],
        entry.params.Simulation.psf_extent_img[1][1]
    ),
    device=entry.params.Hardware.device_simulation,
    roi_size=entry.params.Simulation.roi_size,
    roi_auto_center=entry.params.Simulation.roi_auto_center
)

em_xyz_px=[]
for index_i,z_height in enumerate(range(-500,501,125)):
    em_xyz_px.append([100+index_i*70,100,z_height])

em_xyz_px=torch.tensor(em_xyz_px)
em_phot=torch.ones(len(em_xyz_px))[:]*1000

print(em_xyz_px)

if True:
    if False:
        #too small
        current_psf=psf_roi27
        psf_background=0.5
        psf_threshold=0.05
        z_offset=0
    elif False:
        #too small
        current_psf=psf_roi37
        psf_background=0.4
        psf_threshold=0.0 # todo
        z_offset=0
    elif 0:
        # thise one seems like the best
        current_psf=psf_roi45
        psf_background=0.4
        psf_threshold=0.0 # actually zero
        z_offset=30
    elif True:
        # thise one seems like the best
        current_psf=psf_roi49_d50
        psf_background=0.4
        psf_threshold=0.0 # actually zero
        z_offset=150
    elif False:
        #fits, but lots of empty space
        current_psf=psf_roi63
        psf_background=0.2
        psf_threshold=0.05
        z_offset=0

    em_xyz_px[:,2]+=z_offset
    some_sample_image=current_psf.forward(xyz=em_xyz_px, weight=em_phot)[0,30:130+em_xyz_px.shape[0]*70,0:200]

    plt.subplot(131)
    plt.imshow(some_sample_image,cmap="jet")
    plt.colorbar()

    plt.subplot(132)
    some_sample_image_sum=some_sample_image.sum()
    some_sample_image[some_sample_image>0]-=psf_background
    some_sample_image[some_sample_image<psf_threshold]=0
    some_sample_image*=some_sample_image_sum/some_sample_image.sum()

    plt.imshow(some_sample_image,cmap="jet")
    plt.colorbar()

    plt.subplot(133)
    zrange=range(-600,600,10)
    data=[
        current_psf.forward(xyz=torch.tensor([[0,0,z+z_offset]]), weight=torch.tensor([1000])).max()
        for z in zrange
    ]
    print(f"{numpy.max(data)=}, {zrange[numpy.argmax(data)]}")
    plt.plot(list(range(len(data))),data,marker="x")

    plt.title(f"{psf_threshold=} {psf_background=}")
elif True:
    some_sample_image=psf_roi27.forward(xyz=em_xyz_px, weight=em_phot)[0,30:130+em_xyz_px.shape[0]*70,0:200]
    
    # manually determined from the plots below

    plt.subplot(131)
    plt.imshow(some_sample_image,cmap="jet")
    plt.colorbar()

    plt.subplot(132)
    some_sample_image_sum=some_sample_image.sum()
    some_sample_image[some_sample_image>0]-=psf_background
    some_sample_image[some_sample_image<psf_threshold]=0
    some_sample_image*=some_sample_image_sum/some_sample_image.sum()

    plt.imshow(some_sample_image,cmap="jet")
    plt.colorbar()
else:
    some_sample_image_roi27=psf_roi27.forward(xyz=em_xyz_px, weight=em_phot)[0,30:130+em_xyz_px.shape[0]*70,0:200]
    some_sample_image_roi63=psf_roi63.forward(xyz=em_xyz_px, weight=em_phot)[0,30:130+em_xyz_px.shape[0]*70,0:200]

    plt.subplot(131)
    plt.imshow(some_sample_image_roi27,cmap="jet")
    plt.colorbar()

    plt.subplot(132)
    plt.imshow(some_sample_image_roi63,cmap="jet")
    plt.colorbar()

plt.tight_layout()
plt.show()
