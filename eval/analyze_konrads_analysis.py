#!/usr/bin/env python3

# use pyenv to manage python versions
# $ pyenv install 3.8.13 ; pyenv local 3.8.13 ; pyenv python -m pip install --prefix=$(python -m site --user-base) [-e] /path/to/DECODE/
# source for pip installation: https://stackoverflow.com/questions/41535915/python-pip-install-from-local-dir
# the '--prefix=[..]' is required because setuptools is broken somehow (gives an internal error that int does not have an attribute startswith)
# source for prefix issue: https://github.com/pypa/setuptools/issues/3063

import scipy.io
import numpy
import matplotlib.pyplot as plt
import os
import decode
import torch
from pathlib import Path
from decode.neuralfitter.train.masked_simulation import setup_masked_simulation
import time # for time.sleep
import edt # for edt.edt
from skimage.measure import regionprops,label
from tqdm import tqdm
import pickle # to cache results
from subprocess import Popen, PIPE
import shlex
from decode.utils.entry import Entry

# allows running commands in a different wd using a with statement
class TempWd:
    def __init__(self,wd):
        self.wd=Path(wd)
        assert self.wd.exists() and self.wd.is_dir()
        self.cwd=os.getcwd()
    def __enter__(self):
        os.chdir(str(self.wd))
    def __exit__(self, exception_type, exception_value, exception_traceback):
        os.chdir(self.cwd)

model_path="/home/patrick/code/test_decode/out/new_cell_background/model_2.pt"
param_file_path="/home/patrick/code/test_decode/calibration/params.yaml"
#param_file_path="/home/patrick/code/test_decode/calib/new_cell_background/param_run_in.yaml"

param=decode.utils.read_params(param_file_path)

simulated_z_lower=param.Simulation.emitter_extent[2][0]
simulated_z_upper=param.Simulation.emitter_extent[2][1]
# these are for decode (for matlab/mle i expect -500 to +500)
predicted_z_lower=simulated_z_lower
predicted_z_upper=simulated_z_upper

# setup simulation here because parameters required for auto_scaling need to be derived
sim_train, _ = setup_masked_simulation(param)

#assert sim_train.em_sampler.intensity_dist_type=="discrete"
#param.Simulation.intensity_mu_sig=sim_train.em_sampler._intensity_mu_sig()

# auto-set some parameters (will be stored in the backup copy)
#param = decode.utils.param_io.autoset_scaling(param)

camera = decode.simulation.camera.Photon2Camera.parse(param)

device=torch.device(param.Hardware.device)
#device=torch.device("cpu")

# setup post-processor (from train.setup_trainer)
post_processor = decode.neuralfitter.utils.processing.TransformSequence([
    decode.neuralfitter.scale_transform.InverseParamListRescale(
        phot_max=param.Scaling.phot_max,
        z_max=param.Scaling.z_max,
        bg_max=param.Scaling.bg_max),
    decode.neuralfitter.coord_transform.Offset2Coordinate.parse(param),
    decode.neuralfitter.post_processing.SpatialIntegration(
        raw_th=param.PostProcessingParam.raw_th,
        xy_unit='px',
        px_size=param.Camera.px_size)
])

frame_proc = decode.neuralfitter.scale_transform.AmplitudeRescale.parse(param)

# setup matcher (from train.setup_trainer)
matcher = decode.evaluation.match_emittersets.GreedyHungarianMatching.parse(param)

match_lat=param.Evaluation.dist_lat
match_ax=param.Evaluation.dist_ax

entry=Entry(
    model_file=model_path,
    param_file=param_file_path,
)

def localize_matlab(fluor_filename,*,background_mean_photons,emitter_mean_photons,roiWidth:int=10,noiseTH:int=6):
    matlab_pipeline_dir="/mnt/big_data/code/ImAnalysis"

    # matlab output data is saved to this file
    matlab_pipeline_output_filename="/mnt/big_data/code/test_decode/eval/coordsAll.mat"

    with TempWd(matlab_pipeline_dir):
        matlab_args=[
            f"'coords_mat_out_filename','{matlab_pipeline_output_filename}'",
            f"'image_filenames_match','{fluor_filename}'",
            f"'background_estimation_photons',{background_mean_photons}",
            f"'emitter_estimation_N_photons',{emitter_mean_photons}",
            f"'roiWidth',{roiWidth}",
            f"'noiseTH',{noiseTH}",
        ]
        matlab_command_str=f"""matlab -nosplash -nodesktop -nodisplay -r "run_analysis_from_konrad({",".join(matlab_args)}); exit;" """

        #print(f"running matlab command: {matlab_command_str}")
        with Popen(shlex.split(matlab_command_str),stdout=PIPE,stderr=PIPE,close_fds=True) as matlab_command:
            matlab_command.wait()
            assert matlab_command.returncode==0,f"matlab command failed ({matlab_command.returncode=})"
            matlab_command.kill()

    matlab_output_dict=scipy.io.loadmat("/mnt/big_data/code/test_decode/eval/coordsAll.mat")
    #print(matlab_output_dict.keys())

    coords=matlab_output_dict["coordsAll"]
    emitterAmplitudes=matlab_output_dict["emitterAmplitudes"][0]
    dotRefinements=matlab_output_dict["dotRefinements"][0]

    assert coords.shape[0]==emitterAmplitudes.shape[-1],f"{coords.shape=}, {emitterAmplitudes.shape=}"

    # remove nan coordinates
    coords_mask = numpy.isnan(coords).sum(axis=1)==0
    # remove dots that were refined unsuccessfully
    coords_mask &= ~numpy.isnan(dotRefinements)

    mean_dot_refinement=matlab_output_dict['dotRefinements'][0][coords_mask].mean()
    # values larger than ~2 or something are bad, because they can be an indicator for confusion between dots that are close to each other
    #print(f"matlab mean dot position refinement distance: {mean_dot_refinement:.2f}") 

    dotRefinements=dotRefinements[coords_mask]
    coords=torch.tensor(coords[coords_mask]).float() # xyz
    phot=torch.tensor(emitterAmplitudes[coords_mask])

    # flip x/y axis because coordinate system orientation differs between the matlab and python code
    coords[:,[0,1]]=coords[:,[1,0]]
    # convert coordinates from µm to nm, and flip z axis
    coords[:,2]=coords[:,2]*-1000.0

    ids=torch.arange(0,coords.shape[0]).long()
    frame_indices=torch.zeros(ids.shape).long()

    os.remove(matlab_pipeline_output_filename)

    return decode.generic.emitter.EmitterSet(coords, phot, frame_indices, ids, xy_unit="px", px_size=(65,65))

def generate_frame(simulated_frame_photons_filename,simulated_frame_adu_filename,fraction_emitters_above_zero:float=0.5,override_probs=None):
    emitter_set,mask,frame,_frame_bg=sim_train.sample_full_frame(fraction_emitters_above_zero=fraction_emitters_above_zero,override_probs=override_probs)

    #coords_for_matlab=emitter_set.xyz_nm.numpy()
    #coords_for_matlab[:,[0,1]]=coords_for_matlab[:,[1,0]]
    # x or y needs to be transformed somehow
    #scipy.io.savemat(simulated_frame_adu_filename+".mat", {"expected_coordinates_xyz_nm":coords_for_matlab})

    frame=frame[0].cpu()
    frame_adu=frame.numpy()
    frame_photons=camera.backward(frame).cpu().numpy()

    #save images to disk (for mle, and manual inspection)
    decode.utils.img_file_io.write_img(path=simulated_frame_adu_filename, img_data=frame_adu,from_dtype="float32",as_dtype="float32")
    decode.utils.img_file_io.write_img(path=simulated_frame_photons_filename, img_data=frame_photons,from_dtype="float32",as_dtype="float32")

    #perform edt on mask (create distance label)
    mask_dist=edt.edt(mask!=0, order='C', parallel=0).astype(dtype=numpy.uint8)
    assert mask_dist.min()==0

    max_dist=mask_dist.max()

    #estimate background intensity
    background_photon_estimate=frame_photons[(mask_dist==0)].reshape((-1,)).mean()
    assert background_photon_estimate>0

    #estimate emitter intensity
    emitter_photons=frame_photons.copy()

    background_multiple_threshold:float=2.2 # this value has shown to work well with test cases to eliminate essentially all background noise (though also some dim dots, which means we overestimate the mean dot brightness at lower signal-to-noise ratios)
    min_psf_area:float=150.0

    while True:
        emitter_photons_without_background=emitter_photons-background_photon_estimate # remove background from every pixel, including those containing a dot, because it artificially inflates the dot brightness (if even a bit. it is the right thing to do, goddamnit!)
        emitter_photons_without_background[emitter_photons<(background_photon_estimate*background_multiple_threshold)]=0

        regions=[region for region in regionprops(label(emitter_photons_without_background>0))]
        areas=numpy.array([region.area for region in regions if region.area>=min_psf_area])

        sum_fraction=2 # this factor has shown to scale well to the test-cases

        psf_sums=[]

        for region in regions:
            minr, minc, maxr, maxc = region.bbox

            if region.area<min_psf_area:
                pass
                #emitter_photons_without_background[minr:maxr,minc:maxc]=0 # this is just for visualization (e.g. for debugging, or a paper)
            else:
                current_psf_sum=emitter_photons_without_background[minr:maxr,minc:maxc].sum()/sum_fraction
                psf_sums.append(current_psf_sum)

        if len(psf_sums)>=3:
            break

        background_multiple_threshold*=0.9
        min_psf_area*=0.9

    emitter_photon_estimate=numpy.array(psf_sums).mean()
    #print("emitter_photon_estimate:",emitter_photon_estimate)

    emitter_set.xyz_px=emitter_set.xyz_px.detach().cpu().float()

    #return coordinates and background/emitter estimates
    return emitter_set, frame_photons, frame_adu, background_photon_estimate, emitter_photon_estimate

def eval_model(model_name,fraction_above_zero=0.5,eval_matlab=False,num_frames:int=10,override_probs=None,**matlab_kwargs):
    assert fraction_above_zero>=0.0 and fraction_above_zero<=1.0,"emitter fraction above image plane must be at least 0.0 and at max 1.0"

    result_cache_folder=Path("/mnt/big_data/code/test_decode/eval/cached_comparison_results")
    if not result_cache_folder.exists():
        result_cache_folder.mkdir()

    evaluation_frames_folder=Path("/home/patrick/code/test_decode/eval/evaluation_frames")
    if not evaluation_frames_folder.exists():
        evaluation_frames_folder.mkdir()

    current_file_name=result_cache_folder/f"{model_name}.pickle"
    if current_file_name.exists():
        result=pickle.load(open(current_file_name,"rb"))
    else:
        evaluated_pipelines=["decode","matlab"] if eval_matlab==True else ["decode"]

        simulated_frame_adu_filename="/home/patrick/code/test_decode/eval/evaluation_frames/evaluation_frame_adu{i}.tif"
        simulated_frame_photons_filename="/home/patrick/code/test_decode/eval/evaluation_frames/evaluation_frame_photons{i}.tif"

        result={
            name:{
                "tp":None,
                "fp":None,
                "fn":None,
                "tp_match":None,
            } for name in evaluated_pipelines
        }

        result["meta"]={
            "name":model_name,
            "eval_matlab":eval_matlab,
            "fraction_above_zero":fraction_above_zero,
        }

        total_simulated_emitters=0

        for i in tqdm(range(num_frames),desc=model_name,unit="frames"):
            simulated_emitters, frame_photons, frame_adu, background_photon_estimate, emitter_photon_estimate = generate_frame(
                simulated_frame_photons_filename.format(i=i),
                simulated_frame_adu_filename.format(i=i),
                fraction_emitters_above_zero=fraction_above_zero,
                override_probs=override_probs)

            total_simulated_emitters+=simulated_emitters.xyz_px.shape[0]
                
            for name in evaluated_pipelines:
                if name=="decode":
                    data=entry.localize(frame_adu)
                else:#name=="matlab"
                    data=localize_matlab(simulated_frame_adu_filename.format(i=i),
                                        background_mean_photons=background_photon_estimate,
                                        emitter_mean_photons=emitter_photon_estimate,
                                        **matlab_kwargs)

                data_matched=matcher.forward(output=data,target=simulated_emitters)

                if result[name]["tp"] is not None:
                    result[name]["tp"]=decode.generic.EmitterSet.cat([result[name]["tp"],data_matched.tp])
                    result[name]["fp"]=decode.generic.EmitterSet.cat([result[name]["fp"],data_matched.fp])
                    result[name]["fn"]=decode.generic.EmitterSet.cat([result[name]["fn"],data_matched.fn])
                    result[name]["tp_match"]=decode.generic.EmitterSet.cat([result[name]["tp_match"],data_matched.tp_match])
                else:
                    result[name]["tp"]=data_matched.tp
                    result[name]["fp"]=data_matched.fp
                    result[name]["fn"]=data_matched.fn
                    result[name]["tp_match"]=data_matched.tp_match

        #emitter brightness values: simulated_emitters.phot
        #emitter positions: simulated_emitters.xyz_px (or simulated_emitters.xyz_nm)

        result["meta"]["total_simulated_emitters"]=total_simulated_emitters
        #print(f"background estimate: {background_photon_estimate:.2f} ; brightness estimate: {emitter_photon_estimate:.2f}")

        for name in evaluated_pipelines:
            tp=result[name]["tp"].xyz_nm
            fp=result[name]["fp"].xyz_nm
            fn=result[name]["fn"].xyz_nm
            tp_match=result[name]["tp_match"].xyz_nm

            tp_num=tp.shape[0]
            fp_num=fp.shape[0]
            fn_num=fn.shape[0]
            tp_match_num=tp_match.shape[0]

            distances=((tp-tp_match)**2).sum(axis=1).sqrt()

            #from DECODE "paper" (actually just from their code)
            rmse_lat, rmse_axial, rmse_vol, mad_lat, mad_axial, mad_vol=decode.evaluation.metric.rmse_mad_dist(tp,tp_match)

            result[name]["rmsd"]=rmse_vol
            result[name]["mad"]=mad_vol

            #https://developers.google.com/machine-learning/crash-course/classification/precision-and-recall
            precision=tp_num/(tp_num+fp_num) if (tp_num+fp_num)>0 else float("nan")
            result[name]["precision"]=precision
            recall=tp_num/(tp_num+fn_num) if (tp_num+fn_num)>0 else float("nan")
            result[name]["recall"]=recall
            #https://en.wikipedia.org/wiki/F-score
            result[name]["f1"]=2*precision*recall/(precision+recall)
            num_pred=tp_num+fp_num
            num_expected=tp_num+fn_num
            result[name]["emitter_fraction"]=num_pred/num_expected

            #from DECODE paper
            result[name]["ji"]=tp_num/(tp_num+fp_num+fn_num)

            result[name]["tp_z"]=result[name]["tp_match"].xyz_nm[:,2]
            result[name]["fn_z"]=result[name]["fn"].xyz_nm[:,2]
            result[name]["tp_phot"]=result[name]["tp_match"].phot
            result[name]["fn_phot"]=result[name]["fn"].phot
        
        pickle.dump(result,open(current_file_name,"wb+"))

    return result

def plot_xyz_error_distribution(result):
    plt.figure(figsize=(7.5,13))
    for i_name,name in enumerate([name for name in result.keys() if name!="meta"]):
        if name.lower()!="decode":
            _predicted_z_lower=predicted_z_lower
            _predicted_z_upper=predicted_z_upper
        else:
            _predicted_z_lower=-500
            _predicted_z_upper=500

        method_name=name if name!="matlab" else "mle"

        plt.subplot(2,2,(i_name*2)+1)
        plt.title(method_name)

        hist,_binsx,_binsy=numpy.histogram2d(
            result[name]["tp_match"].xyz_px[:,2].numpy(),
            result[name]["tp"].xyz_px[:,2].numpy(),
            bins=[numpy.arange(simulated_z_lower,simulated_z_upper+1,10),numpy.arange(_predicted_z_lower,_predicted_z_upper+1,10)])

        plt.imshow(numpy.rot90(hist),extent=(simulated_z_lower,simulated_z_upper,_predicted_z_lower,_predicted_z_upper))
        plt.plot([simulated_z_lower,simulated_z_upper],[simulated_z_lower,simulated_z_upper],color="black",linestyle="dashed")
        plt.plot([simulated_z_lower,simulated_z_upper],[simulated_z_lower-match_ax,simulated_z_upper-match_ax],color="black")
        plt.plot([simulated_z_lower,simulated_z_upper],[simulated_z_lower+match_ax,simulated_z_upper+match_ax],color="black")

        # max range that can be predicted
        plt.ylim(_predicted_z_lower,_predicted_z_upper)
        plt.yticks([_predicted_z_lower,_predicted_z_lower/2,0,_predicted_z_upper/2,_predicted_z_upper])

        # max range that is simulated
        plt.xlim(simulated_z_lower,simulated_z_upper)
        plt.xticks([simulated_z_lower,simulated_z_lower/2,0,simulated_z_upper/2,simulated_z_upper])

        plt.ylabel("predicted z [nm]")
        plt.xlabel("real z [nm]")

        plt.subplot(2,2,(i_name*2)+2)
        plt.title(method_name)

        diff=result[name]["tp_match"].xyz_nm[:,:2]-result[name]["tp"].xyz_nm[:,:2]
        diff=diff.numpy()
        hist,_binsx,_binsy=numpy.histogram2d(diff[:,0],diff[:,1],bins=[numpy.arange(-match_lat,match_lat+1,10),numpy.arange(-match_lat,match_lat+1,10)])

        plt.imshow(numpy.rot90(hist),extent=(-match_lat,match_lat,-match_lat,match_lat))
        #plt.plot(circle_x,circle_y,color="black")

        plt.ylim(-match_lat,match_lat)
        plt.yticks([-match_lat,-match_lat/2,0,match_lat/2,match_lat])
        plt.xlim(-match_lat,match_lat)
        plt.xticks([-match_lat,-match_lat/2,0,match_lat/2,match_lat])

        plt.axvline(0,color="black",linestyle="dashed")
        plt.axhline(0,color="black",linestyle="dashed")
        plt.ylabel("dy [nm]")
        plt.xlabel("dx [nm]")

    plt.tight_layout()
    plt.show()

def plot_as_function_of_brightness_and_z(result):
    z_bin_width=20.0
    z_bins=numpy.arange(simulated_z_lower,simulated_z_upper+1,z_bin_width)

    method_colors={"MLE":"orange","DECODE":"green"}

    brightness_bin_width=100.0
    brightness_bins=numpy.arange(0,5051,brightness_bin_width)

    def plot_rmsd_from_z(ax,result):
        ax_l=ax
        ax_r=ax.twinx()

        for name in [name for name in result.keys() if name!="meta"]:
            method_name=(name if name!="matlab" else "mle").upper()

            tp_match_xyz=result[name]["tp_match"].xyz_nm.numpy()

            hist_tp_match,_bins=numpy.histogram(tp_match_xyz[:,2],bins=z_bins)
            hist_fn,_bins=numpy.histogram(result[name]["fn"].xyz_nm.numpy()[:,2],bins=z_bins)

            #bins+=bin_width/2
            hist_total=(hist_tp_match+hist_fn)
            hist_recall=hist_tp_match/hist_total

            difference=result[name]["tp"].xyz_nm.numpy()-tp_match_xyz
            deviation=numpy.sqrt(numpy.sum(difference**2,axis=1))

            tp_match_digitized=numpy.digitize(tp_match_xyz[:,2],z_bins)

            rmsd_list=[]
            for i_bin,_ in enumerate(z_bins):
                tp_mask=tp_match_digitized==i_bin

                if i_bin>0:
                    if tp_mask.any():
                        mean_square_deviation=numpy.mean(deviation[tp_mask]**2)

                        rmsd_list.append(numpy.sqrt(mean_square_deviation))
                    else:
                        rmsd_list.append(0)

            ax_l.plot(z_bins[1:],rmsd_list,label=method_name,color=method_colors[method_name])

        x,y=z_bins[:-1],hist_total
        ax_r.plot(x,y,color="blue")

        ax_r.set_ylabel("num of emitters in this z range",color="blue")
        ax_l.set_ylabel("r.m.s.d. in 3d [nm]")
        ax_l.set_xlabel("emitter z-coordinate [nm]")

        boundary=simulated_z_upper-z_bin_width/2
        ax_r.set_xlim(-boundary,boundary)
        ax_r.set_xticks([-boundary,-250,-100,0,100,250,boundary])

        ax_l.set_ylim(0)
        ax_r.set_ylim(0)

        ax_l.legend(loc="lower right")

    def plot_recall_from_z(ax,result):
        ax_l=ax
        ax_r=ax.twinx()

        for name in [name for name in result.keys() if name!="meta"]:
            method_name=(name if name!="matlab" else "mle").upper()

            hist_tp_match,_bins=numpy.histogram(result[name]["tp_match"].xyz_nm.numpy()[:,2],bins=z_bins)
            hist_fn,_bins=numpy.histogram(result[name]["fn"].xyz_nm.numpy()[:,2],bins=z_bins)

            #bins+=bin_width/2
            hist_total=(hist_tp_match+hist_fn)
            hist_recall=hist_tp_match/hist_total

            x,y=z_bins[:-1],hist_recall
            ax_l.plot(x,y,color=method_colors[method_name],label=method_name)
            ax_l.axvline(x[y.argmax()],color=method_colors[method_name],linestyle="dashed",label=f"max recall ({method_name})")

        ax_l.set_ylabel("recall")
        ax_l.set_ylim(-0.05,1.05)
        ax_l.set_yticks(numpy.linspace(0,1.0,5))

        ax_l.legend(loc="lower right")

        x,y=z_bins[:-1],hist_total
        ax_r.plot(x,y,color="blue")

        ax_r.set_ylabel("num of emitters in this z range",color="blue")
        ax_l.set_xlabel("emitter z-coordinate [nm]")

        boundary=simulated_z_upper-z_bin_width/2
        ax_r.set_xlim(-boundary,boundary)
        ax_r.set_xticks([-boundary,-250,-100,0,100,250,boundary])
        ax_r.set_ylim(0)
    
    def plot_rmsd_from_brightness(ax,result):
        ax_l=ax
        ax_r=ax.twinx()

        for name in [name for name in result.keys() if name!="meta"]:
            method_name=(name if name!="matlab" else "mle").upper()

            tp_match_digits=numpy.digitize(result[name]["tp_match"].phot,bins=brightness_bins)

            difference=result[name]["tp"].xyz_nm.numpy()-result[name]["tp_match"].xyz_nm.numpy()
            deviation=numpy.sqrt(numpy.sum(difference**2,axis=1))

            rmsd_list=[]
            for i_bin,_ in enumerate(brightness_bins):
                tp_mask=tp_match_digits==i_bin

                if i_bin>0:
                    if tp_mask.sum()>0:
                        mean_square_deviation=numpy.mean(deviation[tp_mask]**2)

                        rmsd_list.append(numpy.sqrt(mean_square_deviation))
                    else:
                        rmsd_list.append(0)

            hist_tp,_bins=numpy.histogram(result[name]["tp_phot"],bins=brightness_bins)
            hist_fn,_bins=numpy.histogram(result[name]["fn_phot"],bins=brightness_bins)
            hist_total=(hist_tp+hist_fn)
            hist_tp_fraction=hist_tp/hist_total.clip(min=1)

            x,y=brightness_bins[:-1],numpy.array(rmsd_list)
            ax_l.plot(x,y,color=method_colors[method_name],label=method_name)

        x,y=brightness_bins[:-1],hist_total
        ax_r.plot(x,y,color="blue")
        ax_r.axvline(
            numpy.median(
                numpy.concatenate([result[name]["tp_phot"],result[name]["fn_phot"]])
            ),
            color="blue",linestyle="dashed",
            label="median emitter brightness"
        )

        ax_r.set_ylabel("emitter brightness frequency",color="blue")
        ax_r.set_ylim(0)

        ax_l.set_xlabel("emitter brightness [photons]")
        ax_l.set_xlim(0,5000)
        ax_l.set_xticks([0,500,1000,2000,3000,4000,5000])

        ax_l.set_ylabel("r.m.s.d. in 3d [nm]")

        lines,labels=ax_l.get_legend_handles_labels()
        lines2, labels2 = ax_r.get_legend_handles_labels()
        ax_r.legend(lines + lines2, labels + labels2, loc="upper right")

    def plot_recall_from_brightness(ax,result):
        ax_l=ax
        ax_r=ax.twinx()

        for name in [name for name in result.keys() if name!="meta"]:
            method_name=(name if name!="matlab" else "mle").upper()

            method_color=method_colors[method_name]

            hist_tp,_bins=numpy.histogram(result[name]["tp_phot"],bins=brightness_bins)
            hist_fn,_bins=numpy.histogram(result[name]["fn_phot"],bins=brightness_bins)
            hist_total=(hist_tp+hist_fn)
            hist_tp_fraction=hist_tp/hist_total.clip(min=1)

            y_freq=hist_total
            x,y=brightness_bins[:-1],hist_tp_fraction
            ax_l.plot(x,y,color=method_color,label=method_name)
            ax_l.axvline(x[(numpy.absolute(y-0.5)+((y_freq<100)*1e7)).argmin()],color=method_color,linestyle="dashed",label=f"recall = 0.5 ({method_name})")

        x,y=brightness_bins[:-1],hist_total
        ax_r.plot(x,y,color="blue")
        ax_r.axvline(
            numpy.median(
                numpy.concatenate([result[name]["tp_phot"],result[name]["fn_phot"]])
            ),
            color="blue",linestyle="dashed",
            label="median emitter brightness"
        )

        ax_l.set_ylabel("recall")
        ax_l.set_ylim(-0.05,1.05)
        ax_l.set_yticks([0,0.25,0.5,0.75,1.0])

        ax_r.set_ylabel("emitter brightness frequency",color="blue")
        ax_r.set_ylim(0)

        ax_l.set_xlabel("emitter brightness [photons]")
        ax_l.set_xlim(0,5000)
        ax_l.set_xticks([0,500,1000,2000,3000,4000,5000])

        lines,labels=ax_l.get_legend_handles_labels()
        lines2, labels2 = ax_r.get_legend_handles_labels()
        ax_l.legend(lines + lines2, labels + labels2, loc="lower right")

    plt.figure(figsize=(12,10),dpi=200)
    plt.title(result["meta"]["name"])

    # recall from z
    ax_top_left=plt.subplot(2,2,1)
    # rmsd from z
    ax_top_right=plt.subplot(2,2,3)
    # recall from brightness
    ax_bottom_left=plt.subplot(2,2,2)
    # rmsd from brightness
    ax_bottom_right=plt.subplot(2,2,4)

    plot_rmsd_from_z(ax_top_left, result)
    plot_recall_from_z(ax_bottom_left, result)

    plot_rmsd_from_brightness(ax_top_right, result)
    plot_recall_from_brightness(ax_bottom_right, result)

    plt.tight_layout()#left=0.062,bottom=0.079,right=0.931,top=0.994,wspace=0.35,hspace=0.286)
    plt.show()

def plot_combinations(results):
    plt.figure(figsize=(12,10),dpi=200)
    plot_axes_top_left=plt.subplot(221)
    plot_axes_top_right=plt.subplot(222)

    plot_axes_top_left.set_ylabel("r.m.s.d. in 3D [nm]")
    plot_axes_top_left.set_xlabel("fraction of emitters above image plane")

    plot_axes_bottom_left=plt.subplot(223)
    plot_axes_bottom_left.set_ylabel("r.m.s.d. in 3D [nm]")
    plot_axes_bottom_left.set_xlabel("fraction of emitters above image plane")

    plot_axes_top_right.set_ylabel("recall")
    plot_axes_top_right.set_xlabel("fraction of emitters above image plane")

    plot_axes_bottom_right=plt.subplot(224)
    plot_axes_bottom_right.set_ylabel("recall")
    plot_axes_bottom_right.set_xlabel("fraction of emitters above image plane")

    markers=["x","o","s","v"]
    colors=["red","green","blue","purple"]

    for sim_i,sim_type in enumerate(["real","all1","all2","all4"]):
        fractions=[result["meta"]["fraction_above_zero"] for result in results if result["meta"]["name"].startswith(sim_type)]

        decode_rmsd=[result["decode"]["rmsd"] for result in results if result["meta"]["name"].startswith(sim_type)]
        matlab_rmsd=[result["matlab"]["rmsd"] if ("matlab" in result) else float("nan") for result in results if result["meta"]["name"].startswith(sim_type)]

        decode_recall=[result["decode"]["recall"] for result in results if result["meta"]["name"].startswith(sim_type)]
        matlab_recall=[result["matlab"]["recall"] if ("matlab" in result) else float("nan") for result in results if result["meta"]["name"].startswith(sim_type)]

        plot_axes_top_left    .plot( fractions, decode_rmsd,   marker=markers[sim_i],color=colors[sim_i],    label=f"DECODE ({sim_type})")
        plot_axes_top_right   .plot( fractions, decode_recall, marker=markers[sim_i],color=colors[sim_i], label=f"DECODE ({sim_type})")
        plot_axes_bottom_left .plot( fractions, matlab_rmsd,   marker=markers[sim_i],color=colors[sim_i],   label=f"MLE ({sim_type})")
        plot_axes_bottom_right.plot( fractions, matlab_recall, marker=markers[sim_i],color=colors[sim_i],  label=f"MLE ({sim_type})")

    plot_axes_top_left.    legend(loc="lower right")
    plot_axes_top_right.   legend(loc="lower right")
    plot_axes_bottom_left. legend(loc="lower right")
    plot_axes_bottom_right.legend(loc="lower right")

    plot_axes_top_left.    set_title("DECODE")
    plot_axes_top_right.   set_title("DECODE")
    plot_axes_bottom_left. set_title("MLE")
    plot_axes_bottom_right.set_title("MLE")

    plt.tight_layout()
    plt.show()

if __name__=="__main__":
    all1=[0.0, 1.0, 0.0, 0.0, 0.0]
    all2=[0.0, 0.0, 1.0, 0.0, 0.0]
    all4=[0.0, 0.0, 0.0, 0.0, 1.0]

    num_frames=20

    results=[
        eval_model("realM00", fraction_above_zero=0.0, eval_matlab=True,num_frames=num_frames),
        eval_model("realM03", fraction_above_zero=0.3, eval_matlab=True,num_frames=num_frames),
        eval_model("realM05", fraction_above_zero=0.5, eval_matlab=True,num_frames=100),
        eval_model("realM07", fraction_above_zero=0.7, eval_matlab=True,num_frames=num_frames),
        eval_model("realM10", fraction_above_zero=1.0, eval_matlab=True,num_frames=num_frames),

        eval_model("all1M00",fraction_above_zero=0.0,eval_matlab=True,num_frames=num_frames,override_probs=all1),
        eval_model("all1M03",fraction_above_zero=0.3,eval_matlab=True,num_frames=num_frames,override_probs=all1),
        eval_model("all1M05",fraction_above_zero=0.5,eval_matlab=True,num_frames=num_frames,override_probs=all1),
        eval_model("all1M07",fraction_above_zero=0.7,eval_matlab=True,num_frames=num_frames,override_probs=all1),
        eval_model("all1M10",fraction_above_zero=1.0,eval_matlab=True,num_frames=num_frames,override_probs=all1),

        eval_model("all2M00",fraction_above_zero=0.0,eval_matlab=True,num_frames=num_frames,override_probs=all2),
        eval_model("all2M03",fraction_above_zero=0.3,eval_matlab=True,num_frames=num_frames,override_probs=all2),
        eval_model("all2M05",fraction_above_zero=0.5,eval_matlab=True,num_frames=num_frames,override_probs=all2),
        eval_model("all2M07",fraction_above_zero=0.7,eval_matlab=True,num_frames=num_frames,override_probs=all2),
        eval_model("all2M10",fraction_above_zero=1.0,eval_matlab=True,num_frames=num_frames,override_probs=all2),

        eval_model("all4M00",fraction_above_zero=0.0,eval_matlab=True,num_frames=num_frames,override_probs=all4),
        eval_model("all4M03",fraction_above_zero=0.3,eval_matlab=True,num_frames=num_frames,override_probs=all4),
        eval_model("all4M05",fraction_above_zero=0.5,eval_matlab=True,num_frames=num_frames,override_probs=all4),
        eval_model("all4M07",fraction_above_zero=0.7,eval_matlab=True,num_frames=num_frames,override_probs=all4),
        eval_model("all4M10",fraction_above_zero=1.0,eval_matlab=True,num_frames=num_frames,override_probs=all4),
    ] if 0 else [
        #eval_model("real5r5n4",   fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=5,  noiseTH=4),
        #eval_model("real5r10n4",  fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=10, noiseTH=4),
        #eval_model("real5r15n4",  fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=15, noiseTH=4),

        #eval_model("real5r5n6",  fraction_above_zero=0.5, eval_matlab=True, num_frames=20, roiWidth=5, noiseTH=6),

        #eval_model("real5r7n6",  fraction_above_zero=0.5, eval_matlab=True, num_frames=20, roiWidth=7, noiseTH=6),
        #eval_model("real5r8n6",  fraction_above_zero=0.5, eval_matlab=True, num_frames=20, roiWidth=8, noiseTH=6),
        #eval_model("real5r9n6",  fraction_above_zero=0.5, eval_matlab=True, num_frames=20, roiWidth=9, noiseTH=6),

        #eval_model("real5r10n6", fraction_above_zero=0.5, eval_matlab=True, num_frames=1, roiWidth=10,noiseTH=6),
        #eval_model("real5r15n6",  fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=15, noiseTH=6),

        #eval_model("real5r5n10",  fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=5,  noiseTH=10),
        #eval_model("real5r10n10", fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=10, noiseTH=10),
        #eval_model("real5r15n10", fraction_above_zero=0.5, eval_matlab=True, num_frames=2, roiWidth=15, noiseTH=10),

        #eval_model("realM05", fraction_above_zero=0.5, eval_matlab=True,num_frames=100,roiWidth=5,noiseTH=6),
        #eval_model("realM05_r10n6", fraction_above_zero=0.5, eval_matlab=True,num_frames=100,roiWidth=10,noiseTH=6),

        eval_model("realM05f100", fraction_above_zero=0.5, eval_matlab=False,num_frames=100,roiWidth=5,noiseTH=6),
        #eval_model("realM05f5", fraction_above_zero=0.5, eval_matlab=False,num_frames=5,roiWidth=5,noiseTH=6),
    ]

    print(f"\n{'model':14} {'pipeline':12} {'r.m.s.d.':>12} {'prec':>12} {'rec':>12} {'f1':>12}")
    print("-----------------------------------------------------------------------------------")

    for result in results:
        # plot predicted vs real z heatmap, and localiziation error in dx vs dy
        if 0 and result["meta"]["name"].startswith("real") and result["meta"]["fraction_above_zero"]==0.5:
            plot_xyz_error_distribution(result)

        # print results
        for name in [name for name in result.keys() if name!="meta"]:
            print(f"{result['meta']['name']:14}"\
                f" {name:12}" \
                f" {result[name]['rmsd']:12.3f}" \
                f" {result[name]['precision']:12.3f}" \
                f" {result[name]['recall']:12.3f}" \
                f" {result[name]['f1']:12.3f}")

    if 1:
        plot_combinations(results)

    if 1:
        for result in [r for r in results if r["meta"]["name"].startswith("real") and r["meta"]["fraction_above_zero"]==0.5]:
            plot_as_function_of_brightness_and_z(result)


#erik for questions about smeagol
