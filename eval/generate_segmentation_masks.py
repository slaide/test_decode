import decode
from decode.utils.gen_masks import generate_cell_masks

import glob
from pathlib import Path

# masks should go into /crex/proj/uppstore2018129/elflab/PersonalFolders/Patrick/segmentation_masks_flowcell

all_dirs=[]

project_base_path=Path("/home/patrick/code/test_decode")

param = decode.utils.read_params(str(project_base_path/"calibration/params.yaml"))
param_fluo_roi_original=param.Simulation.fluo_roi
for s in [
    str(project_base_path/"experiments/BQ4399_with_dots/EXP-22-BQ4399/Run/Pos*"),
    str(project_base_path/"experiments/BQ4383_bg_only/EXP-22-BQ4383/Run/Pos*"),
]:
    matched_folders=glob.glob(s)

    all_dirs.extend(matched_folders)

param.Simulation.fluo_roi=param_fluo_roi_original
print("segmenting channel 1")
generate_cell_masks(
    directory_list=all_dirs,
    fluo_roi=param.Simulation.fluo_roi,
    fluor_dir_name="fluor515",
    dir_phase="phase",

    fluor_cropped_out_dir_name=None,
    dir_warped_dist_masks=(True,"/home/patrick/code/test_decode/dist_masks"),
    warped_dist_masks_prefix="mask_515_",

    transmat_file_name=str(project_base_path/"calibration/transMatV_3D.mat"),
    model_path=str(project_base_path/"calibration/mixed10epochs_betterscale_contrastAdjusted1.pth"))

# crop the other channel (by just shifting <image height>/2 upwards) -> better solution is to use transMatV/C.mat
param.Simulation.fluo_roi=(
    param.Simulation.fluo_roi[0],
    (
        param.Simulation.fluo_roi[1][0]-(2082//2),
        param.Simulation.fluo_roi[1][1]-(2082//2)
    ),
)

print("segmenting channel 2")
generate_cell_masks(
    directory_list=all_dirs,
    fluo_roi=param.Simulation.fluo_roi,
    fluor_dir_name="fluor580",
    dir_phase="phase",

    fluor_cropped_out_dir_name=None,
    dir_warped_dist_masks=(True,"/home/patrick/code/test_decode/dist_masks"),
    warped_dist_masks_prefix="mask_580_",

    transmat_file_name=str(project_base_path/"calibration/transMatC_3D.mat"),
    model_path=str(project_base_path/"calibration/mixed10epochs_betterscale_contrastAdjusted1.pth"))