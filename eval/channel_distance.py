import decode
from decode.utils.entry import Entry
from tqdm import tqdm
import scipy.io
import numpy
import matplotlib.pyplot as plt
from pathlib import Path
import skimage
import cv2 as cv
import torch
import skimage.io
from decode.generic.emitter import EmitterSet

model_list=[
    "2022-08-26_20-49-54_patrick-workstation_psf1n2_norotaug",
    "2022-08-27_13-48-25_patrick-workstation_psf1_only",
    "2022-08-27_14-41-21_patrick-workstation_psf2_only",
    "2022-08-27_15-34-44_patrick-workstation_psf1n2_rotaug",
]

model_data=[]
for model_index,model in enumerate(model_list):
    print(f"using model: {model}")

    general_model=Entry(
        model_file=f"/home/patrick/code/test_decode/out/{model}/model_2.pt",
        param_file=f"/home/patrick/code/test_decode/out/{model}/param_run_in.yaml",
        device="cuda",
        threshold=0.1
    )

    entry515=general_model
    entry580=general_model

    channel515_limits=(262,1140,1081,2080)
    channel580_limits=(262,1140-2082//2,1081,2080-2082//2)

    max_match_dist=300
    bins=numpy.linspace(0,max_match_dist,100)

    image_names_515=[]
    image_names_580=[]
    emitters_515=None
    emitters_580=None

    for pos_index in tqdm(range(1,33 if 0 else 2),desc="pos"):
        for image_index in tqdm(range(601),desc="image",leave=False):
            for channel in [515,580]:
                img_path=f"/home/patrick/code/test_decode/experiments/BY8853_with_dots/EXP-22-BY8853/Run/Pos{pos_index:02}/fluor{channel}/img_{image_index:09}.tiff"
                image_adu=skimage.io.imread(img_path, plugin="tifffile", as_gray = True)

                if channel==515:
                    minx,miny,maxx,maxy=channel515_limits
                    coords=entry515.localize(image_adu[miny:maxy,minx:maxx])
                    coords.frame_ix[:]=len(image_names_515)

                    image_names_515.append(img_path)

                    if emitters_515 is None:
                        emitters_515=coords
                    else:
                        emitters_515=EmitterSet.cat([emitters_515,coords])
                else:
                    minx,miny,maxx,maxy=channel580_limits
                    coords=entry580.localize(image_adu[miny:maxy,minx:maxx])
                    coords.frame_ix[:]=len(image_names_580)

                    image_names_580.append(img_path)

                    if emitters_580 is None:
                        emitters_580=coords
                    else:
                        emitters_580=EmitterSet.cat([emitters_580,coords])

                if False and channel==580:
                    coords515=emitters_515[emitters_515.frame_ix==(len(image_names_515)-1)]
                    coords580=emitters_580[emitters_580.frame_ix==(len(image_names_580)-1)]

                    image_adu_515=skimage.io.imread(f"/home/patrick/code/test_decode/experiments/BY8853_with_dots/EXP-22-BY8853/Run/Pos{pos_index:02}/fluor515/img_{image_index:09}.tiff",as_gray=True)
                    image_adu_580=image_adu

                    mat_c=scipy.io.loadmat("../calibration/transMatC_3D.mat")["transformationMatrix"].T
                    mat_v=scipy.io.loadmat("../calibration/transMatV_3D.mat")["transformationMatrix"].T

                    inv_c=numpy.linalg.inv(mat_c)
                    inv_v=numpy.linalg.inv(mat_v)

                    coords580=coords580.xyz_px[:,[1,0,2]].numpy()
                    coords515=coords515.xyz_px[:,[1,0,2]].numpy()
                    
                    if len(coords580)>0:
                        coords580_in515space=points=cv.perspectiveTransform(cv.perspectiveTransform(coords580[:,:2].reshape(1,-1,2),mat_c),inv_v)[0]
                    
                    if len(coords515)>0:
                        coords515_in580space=points=cv.perspectiveTransform(cv.perspectiveTransform(coords515[:,:2].reshape(1,-1,2),mat_v),inv_c)[0]
                    
                    if len(coords515)==0 or len(coords580)==0:
                        continue

                    do_plot=True

                    minx,miny,maxx,maxy=channel580_limits
                    if do_plot:
                        ax=plt.subplot(121)
                        ax.set_title("580")

                        image_adu_580[miny:maxy,minx:maxx]*=2
                        plt.imshow(image_adu_580,extent=(0,image_adu_580.shape[1],image_adu_580.shape[0],0))
                        plt.ylim(image_adu_580.shape[0],0)
                        plt.xlim(0,image_adu_580.shape[1])

                        #plt.scatter(coords580[:,0]+minx,coords580[:,1]+miny,color="red",marker="x")

                    minx,miny,maxx,maxy=channel515_limits
                    if do_plot:
                        ax=plt.subplot(122)
                        ax.set_title("515")

                        image_adu_515[miny:maxy,minx:maxx]*=2
                        plt.imshow(image_adu_515,extent=(0,image_adu_515.shape[1],image_adu_515.shape[0],0))
                        plt.ylim(image_adu_515.shape[0],0)
                        plt.xlim(0,image_adu_515.shape[1])

                        #plt.scatter(coords515[:,0]+minx,coords515[:,1]+miny,color="red",marker="x")

                    # perform emitter matching for performance evaluation
                    matcher=decode.evaluation.match_emittersets.GreedyHungarianMatching(match_dims=2,dist_lat=max_match_dist)
                    emitters_515_xyz=coords515
                    emitters_580_xyz=numpy.zeros_like(coords580)
                    emitters_580_xyz[:,:2]=coords580_in515space
                    
                    tp,fp,fn,tp_match=matcher.forward(
                        output=decode.generic.EmitterSet(
                            torch.from_numpy(emitters_515_xyz), 
                            torch.zeros((emitters_515_xyz.shape[0])), 
                            torch.zeros((emitters_515_xyz.shape[0]),dtype=torch.long),
                            id=torch.arange(0,emitters_515_xyz.shape[0],dtype=torch.long),
                            xy_unit="px",px_size=(65,65)
                        ),
                        target=decode.generic.EmitterSet(
                            torch.from_numpy(emitters_580_xyz), 
                            torch.zeros((emitters_580_xyz.shape[0])), 
                            torch.zeros((emitters_580_xyz.shape[0]),dtype=torch.long),
                            id=torch.arange(0,emitters_580_xyz.shape[0],dtype=torch.long),
                            xy_unit="px",px_size=(65,65)
                        ),
                    )

                    if do_plot:
                        plt.show()

                    distances=numpy.sqrt(numpy.sum((tp.xyz_nm-tp_match.xyz_nm).numpy()[:,:2]**2,axis=1))
                    hist_,_bins=numpy.histogram(distances,bins=bins)

                    if pos_index==1 and image_index==0:
                        hist=hist_
                    else:
                        hist+=hist_

    # image file names: mat=scipy.io.loadmat("distance_stuff.mat"); 
    # # mat["Pos01"][0,0].dtype.fields.keys() 
    # -> mat["Pos01"][0,0][image_index][0,0].dtype.fields.keys()
    #  or mat["Pos01"][0,0][image_index][0,0][channel_index]
    if 0:
        scipy.io.savemat("two_channel_localizations.mat",coords_list)

    minx,miny,maxx,maxy=channel515_limits
    emitters_515.xyz_px[:,0]+=miny
    emitters_515.xyz_px[:,1]+=minx
    minx,miny,maxx,maxy=channel580_limits
    emitters_580.xyz_px[:,0]+=miny
    emitters_580.xyz_px[:,1]+=minx

    model_data.append((emitters_580,emitters_515))

mat_c=scipy.io.loadmat("../calibration/transMatC_3D.mat")["transformationMatrix"].T
mat_v=scipy.io.loadmat("../calibration/transMatV_3D.mat")["transformationMatrix"].T

inv_c=numpy.linalg.inv(mat_c)
inv_v=numpy.linalg.inv(mat_v)

matcher=decode.evaluation.match_emittersets.GreedyHungarianMatching(match_dims=2,dist_lat=max_match_dist)

threshold_list=[0.1,0.2,0.3,0.4,0.5,0.7]
colors=["orange","lightblue","green","red","purple","pink","lightgreen","yellow"]

threshold_data=[]

model_transformed_coords=[]

for emitters_580, emitters_515 in model_data:
    # clone data to allow (mostly) in-place transformation
    emitters_580_in_515_space=emitters_580.clone()
    # transform coordinates into space of other coordinate set (switch x/y before and after transformation because thats how the matrix multiplication works)
    emitters_580_in_515_space.xyz_px[:,[1,0]]=torch.from_numpy(cv.perspectiveTransform(
        cv.perspectiveTransform(
            emitters_580_in_515_space.xyz_px[:,[1,0]].reshape(1,-1,2).cpu().numpy(),
        mat_c),
    inv_v)[0])

    #coords515_in580space=points=cv.perspectiveTransform(cv.perspectiveTransform(emitters_515.xyz_px[:,[1,0,2]][:,:2].reshape(1,-1,2),mat_v),inv_c)[0]

    model_transformed_coords.append((
        emitters_515,
        emitters_580_in_515_space
    ))

for threshold in tqdm(threshold_list,desc="th. model"):
    model_results=[]

    for emitters_515, emitters_580_in_515_space in tqdm(model_transformed_coords,desc="th. apply",leave=False):
        # perform emitter matching for performance evaluation
        tp,fp,fn,tp_match=matcher.forward(
            output=emitters_515[emitters_515.prob>=threshold],
            target=emitters_580_in_515_space[emitters_580_in_515_space.prob>=threshold],
        )

        matching_fraction=len(tp)/(len(tp)+len(fp)+len(fn))

        distances=tp.xyz_nm[:,:2].numpy()-tp_match.xyz_nm[:,:2].numpy()
        distances=numpy.sqrt(numpy.sum(distances**2,axis=1))

        median_distance=numpy.median(distances)

        num_matched=len(tp)

        model_results.append((matching_fraction,median_distance,num_matched))
    
    threshold_data.append(model_results)

ax_median_distance=plt.subplot(211)
ax_matching_fraction=ax_median_distance.twinx()
ax_num_matched=plt.subplot(212)
for model_index,_ in enumerate(model_list):
    color=colors[model_index]
    ax_matching_fraction.plot(
        threshold_list,
        [results[model_index][0] for results in threshold_data],
        c=color,
        label=model_list[model_index],
        linestyle="solid")
    ax_median_distance.plot(
        threshold_list,
        [results[model_index][1] for results in threshold_data],
        c=color,
        label=model_list[model_index],
        linestyle="dashed")
    ax_num_matched.plot(
        threshold_list,
        [results[model_index][2] for results in threshold_data],
        c=color,
        label=model_list[model_index])

ax_matching_fraction.set_ylabel("matched fraction of emitters")
ax_median_distance.set_ylabel("median distance [nm]")
ax_median_distance.set_xlabel("confidence threshold")
ax_matching_fraction.legend()
ax_num_matched.set_xlabel("confidence threshold")
ax_num_matched.set_ylabel("rel. freq.")
ax_num_matched.legend()
plt.show()