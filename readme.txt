# useful cli commands:
copy files with : rsync -rh[z] --progress --size-only --include="Pos*0" --include="Pos*1" --include="Pos*2" --exclude="Pos*" phennig@rackham.uppmax.uu.se:/crex/proj/uppstore2018129/elflab/path experiments/path
check size of folder: du -hs foldername




# some data i have used:
    bead z stack:              /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4393/zStack_3D_101 # 25nm
                            /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-21-BQ4373/fluorChannelRegistration3D/zStack_3D10{1,2} # 50nm
    brightness:                /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4383
    experimental images:       /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4399/cherryIn3D/Cherry
    mle calibration curve:     /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4392calibrationCurves26-67.mat
    membrane experiments:      /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4393/Run/Pos11/Pos0/fluor580
    transMat_C/transMat_V.mat: /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4384

    real images: for each position Venus (bottom channel) and Cherry (top channel)
    calibration curves: the z-coordinate based on interpolation of the sigma values from Gaussian fitting. "The 26-67 just refers to the image frames in the image stack I used to make the calibration curves"




# data i have made:
    /crex/proj/uppstore2018129/elflab/PersonalFolders/Patrick/psf_data
        psf data (bead stacks as separate images and as image stacks)
        25_BQ4393_1
            25nm between images in stack
            source: /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-22-BQ4393/zStack_3D_101
            45px roi size (psf approximation volume width)
        50_BQ4373_1
            50nm between images in stack
            source: /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-21-BQ4373/fluorChannelRegistration3D/zStack_3D101
            45px roi size (psf approximation volume width)
        50_BQ4373_2
            50nm between images in stack
            source: /crex/proj/uppstore2018129/elflab/Projects/Chromosome_structure/EXP-21-BQ4373/fluorChannelRegistration3D/zStack_3D102
            45px roi size (psf approximation volume width)
    /crex/proj/uppstore2018129/elflab/PersonalFolders/Patrick/models
        trained ai models
        - psf model #1
        - psf model #2
        - psf model #1 & #2
        - psf model #1 & #2 & rotation augmentation
    /crex/proj/uppstore2018129/elflab/PersonalFolders/Patrick/segmentation_masks_flowcell
        segmentation masks for ai training



# noteable code:
eval/
    approximate_background.py:
        approximate environmental background, blur radius and brightness per voxel inside the cell, using a probably not very good optimizer (though runs fast, and plots results in real-time)
        should not be required anymore since AI can be trained on whole parameter space with no apparent downsides in terms of localization performance
    channel_distance.py:
        apply a list of ai models to some experimental data and compare the results
        based on the 'two-channel' experiment
    check_roi_size.py:
        some code to check psf size and background removal values
    generate_segmentation_masks.py:
        example usage of decode.utils.gen_masks (approximate_background.py also contains one, in a different context)
        generate segmentation mask from phase contrast images in multiple folders at the same time
    split_fit_psf.py:
        attempts at grouping together dots/psfs from multiple beads from the same stack
        based on some similarity metric between the dot shapes (rmsd)
        this is waiting for some code to automatically generate a psf approximation with the psf approximation code that is currently integrated into smap (partially)




# setup and training instructions
generate PSF approximation using SMAP (instructions from section 5.4 of https://www.embl.de/download/ries/Documentation/SMAP_UserGuide.pdf#page=9)
    install SMAP from https://rieslab.de/#software (either binary version or matlab source code. binary is available for macOS and windows, matlab source code might also work on linux)
    open smap
    setup the camera data:
        in smap, on the left in the top bar, click on SMAP -> Camera manager
        make sure that in the table in the top left of the new window, the line with the camera name "Default" is selected (will be highlighted in cyan) (do not change this camera name in the table!)
        change the parameters accordingly:
            change value in column 'mode' to 'fix' for all parameters (except those below)
            set 'fixvalue' to the corresponding values
            for numberOfFrames/Width/Height: choose mode 'metadata'
        click save in bottom right
        then click close
    to run psf approximation:
        click in the top bar: Plugins -> Analyze -> sr3D -> calibrate3DsplinePSF
        in the new window, click run (then close the window that contains the run button. dont worry, this will not close the window that just poped up)
        in the new new window:
            'select camera files' actually refers to the bead stack, with the whole stack contained in a single file. click on this.
            click on 'add' and select the bead z-stack .tif/.tiff file, then click done
                each file needs to be a single stack
                you can add more than 1 stack to decrease the noise included in the psf approximation, but all parameters, incl. distance between planes, needs to be identical
            if a message pops up that says 'Camera not recognized. Use default camera?', click yes (this message may pop up later, too. clock yes then, too)
            you can/should edit the location of the output file before you proceed
            3D modality should be set to 'arbitrary' (leave the 'bi dir' checkbox unchecked)
            set the distance between frames (in nm)
            [ you can also change the minimum distance (between beads), though it should not be necessary ]
            set the "frames to use for cc" to a value that is described in its pop-up explanation (a number of frames corresponding to 300-1500 nm, given the distance between frames. e.g. set to 15 with 50nm between frames)
            leave everything else as is, except:
            set the ROI size to a size that fits well. manual observations may show that the dots from the simulated psf look truncted in a square pattern -> increase ROI size to potentially solve this (also, size value should be odd. if there is an error where "no beads could be found. maybe roi size is too large?", this might actually be caused by an even roi size, not actually the size value)
            click 'Calculate bead calibration' at the bottom. a new window will pop up. Leave it open and wait until the new window contains tabs at the top, called "Files", "PSFz", "PSFx", "validate", and "CRLB" (there will be some text at the bottom of the window that contains all the parameters you just specified, it will tell you what it is currently doing, and when it is done)
            (you can interact with the window while some tabs are still missing)
            make sure that the approximation in z/x looks good (if not, tweak some parameters, e.g. distance between beads or ROI size)
            the maximum z range that can be used from this approximation is visible in the tab "validate". Each line corresponds to a single emitter that was localized using the psf approximation. the frame range between the noisy regions can be used (lines looking offset from each other is not great, but also not easily solveable currently).




generate new segmentation masks for training:
    see eval/generate_segmentation_masks.py




train new AI model:
    edit params.yaml
    [ run approximate_background.py ] optional
    adjust params.yaml using the data gained from background approximation
    train ai using $ pyenv exec python -m decode.neuralfitter.train.train -p params.yaml -l <the folder name that is specified in the params.yaml file, under InOut.experiment_out. this argument here is the folder where the logs will go, which should be in the same folder as the model, which will be placed in the folder specified in the params.yaml file> -c <some descriptive name for this model. the output folder for the model will be params.InOut.experiment_out/combinationOfDatetimeAndComputerName_theStringSpecifiedHere, so e.g. with -c mysuperspecialmodel and InOut.experiment_out=myAiModels -> myAiModels/2022-08-14_12-45-32_mycomputer_mysuperspecialmodel>
    observe training progress in terminal and/or $ tensorboard --samples_per_plugin images=100 --port=6006 --logdir=<params.InOut.experiment_out> (then open browser with link shown after running this command)

    ...

    apply model using decode.utils.entry e.g. $ pyenv exec python -m decode.utils.entry aiModels/myModel/model_2.pt aiModels/myModel/param_run_in.yaml experiments/myExperiment/Pos42/img12345.tif -o results/myExperiment/img12345.tif
